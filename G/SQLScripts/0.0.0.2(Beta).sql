﻿---- update for 0.0.0.2(Beta) ----

ALTER TABLE [dbo].[DeployFlowProperty]
    ADD [Host] INT DEFAULT 1 NOT NULL;

ALTER TABLE [dbo].[DeployFlowProperty]
    ADD [DeployMode] INT DEFAULT 1 NOT NULL;

ALTER TABLE [dbo].[DeployFlowProperty]
    ADD [JsonHostProperty] INT DEFAULT '' NOT NULL;

UPDATE DeployFlowProperty SET JsonHostProperty = '{
    "WebsiteName": "'+DeployProject.HostProperty+'",
 }' 
 FROM dbo.DeployProject
 WHERE DeployProjectID = DeployProject.ID

ALTER TABLE [DeployProject]  DROP COLUMN [Host]

ALTER TABLE [DeployProject]  DROP COLUMN [HostProperty]

ALTER TABLE [DeployProject]  DROP COLUMN [AbsolutePath]