﻿using Framework.Common.LogOperation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace G.Infrastructure.Windows.ProcessOperation
{
    public class ProcessWrapper
    {
        private List<string> _lstError = new List<string>();
        private List<string> _lstOutput = new List<string>();

        public int Run(string fileName, string arguments, bool hideWindow = false)
        {
            Logger.Info($"Run Process Output:\r\n\tFileName:{fileName}\r\n\tArguments:{arguments}\r\n\tHideWindow:{hideWindow}");

            return ProcessHelper.StartProcessAsCurrentUser(fileName, arguments, visible: !hideWindow);
        }

        public void RunAndCheckOutputError(string fileName, string arguments)
        {
            _lstError.Clear();
            _lstOutput.Clear();

            using (var process = new Process())
            {
                var processStartInfo = new ProcessStartInfo();
                processStartInfo.FileName = fileName;
                processStartInfo.Arguments = arguments;

                processStartInfo.UseShellExecute = false;
                processStartInfo.CreateNoWindow = true;
                processStartInfo.RedirectStandardOutput = true;
                processStartInfo.RedirectStandardError = true;

                process.StartInfo = processStartInfo;
                process.ErrorDataReceived += Process_ErrorDataReceived;
                process.OutputDataReceived += Process_OutputDataReceived;
                process.Exited += Process_Exited;
                process.Start();
                process.BeginErrorReadLine();
                process.BeginOutputReadLine();

                while (!process.HasExited)
                {
                    Thread.Sleep(20);
                }

                Logger.Info("RunAndCheckOutputError Output:" + string.Join("\r\n", _lstOutput));
            }
        }

        public void RunAndCheckErrorByFilter(string fileName, string arguments, string successFilter)
        {
            using (var process = new Process())
            {
                var processStartInfo = new ProcessStartInfo();
                processStartInfo.FileName = fileName;
                processStartInfo.Arguments = arguments;

                processStartInfo.UseShellExecute = false;
                processStartInfo.CreateNoWindow = true;
                processStartInfo.RedirectStandardOutput = true;
                processStartInfo.RedirectStandardError = true;

                process.StartInfo = processStartInfo;
                process.Start();

                string output = process.StandardOutput.ReadToEnd();

                if (!string.IsNullOrEmpty(successFilter))
                {
                    if (!output.Contains(successFilter))
                    {
                        var errorMessage = $"RunAndCheckErrorByFilter Error:{output}";
                        Logger.Error(errorMessage);
                        throw new Exception(errorMessage);
                    }
                }

                Logger.Info("RunAndCheckErrorByFilter Output:" + output);

                process.WaitForExit();
            }
        }

        private void Process_Exited(object sender, EventArgs e)
        {
            if (_lstError.Count == 0)
                return;

            string errorMessage = string.Join("\r\n", _lstError);

            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                Logger.Error(errorMessage);

                throw new Exception(errorMessage);
            }
        }

        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            _lstOutput.Add(e.Data);
        }

        private void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            _lstError.Add(e.Data);
        }

        public List<Process> GetProcessInfoList(string filePath)
        {
            var wmiQueryString = "SELECT ProcessId, ExecutablePath, CommandLine FROM Win32_Process";
            using (var searcher = new ManagementObjectSearcher(wmiQueryString))
            using (var results = searcher.Get())
            {
                var query = from p in Process.GetProcesses()
                            join mo in results.Cast<ManagementObject>()
                            on p.Id equals (int)(uint)mo["ProcessId"]
                            select new
                            {
                                Process = p,
                                ExeFilePath = (string)mo["ExecutablePath"],
                            };

                return query.Where(pi => pi.ExeFilePath == filePath).Select(pi => pi.Process).ToList();
            }
        }

        public void Kill(string filePath)
        {
            GetProcessInfoList(filePath).ForEach(process =>
            {
                process.Kill();

                process.WaitForExit();
            });
        }
    }
}
