﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace G.Infrastructure.Security
{
    public class MD5
    {
        public static string GetEncryptPassword(string password)
        {
            return Encrypt(Encrypt(password));
        }

        public static string Encrypt(string data)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                byte[] fromData = Encoding.UTF8.GetBytes(data);
                byte[] targetData = md5.ComputeHash(fromData);
                StringBuilder sb = new StringBuilder(targetData.Length);

                for (int i = 0; i < targetData.Length; i++)
                {
                    sb.Append(targetData[i].ToString("x"));
                }

                return sb.ToString();
            }
        }
    }
}