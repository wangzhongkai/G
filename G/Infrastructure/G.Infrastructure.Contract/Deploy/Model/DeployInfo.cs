﻿using G.Infrastructure.Constant.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Contract.Deploy.Model
{
    public class DeployInfo
    {
        public string FilePackageName { get; set; }

        public DeployProjectType DeployProjectType { get; set; }

        public DeployHostType DeployHostType { get; set; }

        public DeployMode DeployMode { get; set; }

        public string JsonHostProperty { get; set; }
    }
}
