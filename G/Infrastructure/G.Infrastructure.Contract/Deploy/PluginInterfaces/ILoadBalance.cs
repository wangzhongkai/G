﻿using G.Infrastructure.Constant.Enums;
using G.Infrastructure.Contract.Deploy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Contract.Deploy.PluginInterfaces
{
    /// <summary>
    /// 负载均衡接口
    /// </summary>
    public interface ILoadBalance
    {
        Task<int> SetServerWeight(string loadBalanceValue, string serverIdentity, int weight);

        Task<int> GetServerWeight(string loadBalanceValue, string serverIdentity);
    }
}