﻿using G.Infrastructure.Contract.Deploy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Contract.Deploy.PluginInterfaces
{
    public interface IDeployHost
    {
        void Deploy(DeployInfo deployInfo);
    }
}
