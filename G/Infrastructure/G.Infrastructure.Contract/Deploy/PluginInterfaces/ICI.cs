﻿using G.Infrastructure.Constant.Enums;
using G.Infrastructure.Contract.Deploy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Contract.Deploy.PluginInterfaces
{
    /// <summary>
    /// 持续集成接口
    /// </summary>
    public interface ICI
    {
        Task<string> BuildJob(string jsonConfig);

        Task<CIJobStatus> GetStatusByJobNumber(string jsonConfig, string jobNumber);

        Task<ArtifactInfo> GetArtifactInfo(string jsonConfig, string jobNumber, string artifactName);

        Task<string> GetConsoleUrlByJobNumber(string jsonConfig, string jobNumber);
    }
}