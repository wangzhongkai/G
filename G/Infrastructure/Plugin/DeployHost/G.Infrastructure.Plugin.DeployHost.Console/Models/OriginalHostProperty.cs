﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Plugin.DeployHost.Console.Models
{
    public class OriginalHostProperty
    {
        public string ExeFilePath { get; set; }

        public string Directory { get; set; }
    }
}
