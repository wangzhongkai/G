﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using G.Infrastructure.Contract.Deploy.Model;
using G.Infrastructure.Contract.Deploy.PluginInterfaces;
using G.Infrastructure.Constant.Enums;
using G.Infrastructure.Windows.ProcessOperation;
using Framework.Common.ZipOperation;
using Newtonsoft.Json;
using G.Infrastructure.Plugin.DeployHost.Console.Models;
using System.Reflection;
using System.IO;

namespace G.Infrastructure.Plugin.DeployHost.Console
{
    public class ConsoleImpl : IDeployHost
    {
        public void Deploy(DeployInfo deployInfo)
        {
            if (deployInfo.DeployMode != DeployMode.Original)
                throw new NotImplementedException("目前仅支持[原始]部署方式");

            #region Parse Parameters

            var rootDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var deployDirectory = Path.Combine(rootDirectory, "Download", "Deploy");

            var deployFilePackagePath = Path.Combine(deployDirectory, deployInfo.FilePackageName);

            var originalHostProperty = JsonConvert.DeserializeObject<OriginalHostProperty>(deployInfo.JsonHostProperty);

            #endregion

            var processWrapper = new ProcessWrapper();
            processWrapper.Kill(originalHostProperty.ExeFilePath);

            ZipHelper.Unzip(deployFilePackagePath, originalHostProperty.Directory);
        }
    }
}