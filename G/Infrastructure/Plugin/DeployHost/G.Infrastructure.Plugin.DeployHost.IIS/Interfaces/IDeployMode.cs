﻿using G.Infrastructure.Contract.Deploy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Plugin.DeployHost.IIS.Interfaces
{
    public interface IDeployWorker
    {
        void Deploy(string deployFilePackagePath, string jsonHostProperty);
    }
}