﻿using Framework.Common.ZipOperation;
using G.Infrastructure.Plugin.DeployHost.IIS.Interfaces;
using G.Infrastructure.Plugin.DeployHost.IIS.Models;
using G.Infrastructure.Windows.ProcessOperation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Plugin.DeployHost.IIS.DeployModes
{
    public class OriginalDeployWorker : IDeployWorker
    {
        public void Deploy(string deployFilePackagePath, string jsonHostProperty)
        {
            var originalHostProperty = JsonConvert.DeserializeObject<OriginalHostProperty>(jsonHostProperty);

            ZipHelper.Unzip(deployFilePackagePath, originalHostProperty.AbsolutePath);

            List<Tuple<string, string, string>> lstCommand = new List<Tuple<string, string, string>>();

            lstCommand.Add(new Tuple<string, string, string>(@"C:\Windows\SysWOW64\inetsrv\appcmd.exe", $"recycle apppool /apppool.name:{originalHostProperty.AppPoolName}", "成功"));

            var processWrapper = new ProcessWrapper();

            lstCommand.ForEach(command =>
            {
                processWrapper.RunAndCheckErrorByFilter(command.Item1, command.Item2, command.Item3);
            });

            System.Threading.Thread.Sleep(1000);

            //在Config文件末尾增加控制，触发IIS重启网站
            var originalConfigFilePath = Path.Combine(originalHostProperty.AbsolutePath, "Web.config");
            File.AppendAllText(originalConfigFilePath, " ", Encoding.UTF8);
        }
    }
}