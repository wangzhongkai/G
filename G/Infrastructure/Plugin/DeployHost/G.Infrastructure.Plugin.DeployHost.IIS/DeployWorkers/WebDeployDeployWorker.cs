﻿using G.Infrastructure.Plugin.DeployHost.IIS.Interfaces;
using G.Infrastructure.Plugin.DeployHost.IIS.Models;
using G.Infrastructure.Windows.ProcessOperation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Plugin.DeployHost.IIS.DeployModes
{
    public class WebDeployDeployWorker : IDeployWorker
    {
        public void Deploy(string deployFilePackagePath, string jsonHostProperty)
        {
            var webDeployHostProperty = JsonConvert.DeserializeObject<WebDeployHostProperty>(jsonHostProperty);

            new ProcessWrapper().RunAndCheckOutputError(
                @"C:\Program Files\IIS\Microsoft Web Deploy V3\msdeploy.exe",
                $@"-verb:sync -source:package=""{deployFilePackagePath}"" -dest:contentpath=""{webDeployHostProperty.WebsiteName}""");
        }
    }
}