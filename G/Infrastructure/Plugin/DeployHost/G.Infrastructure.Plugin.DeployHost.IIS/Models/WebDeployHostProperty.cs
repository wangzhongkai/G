﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Plugin.DeployHost.IIS.Models
{
    public class WebDeployHostProperty
    {
        public string WebsiteName { get; set; }
    }
}
