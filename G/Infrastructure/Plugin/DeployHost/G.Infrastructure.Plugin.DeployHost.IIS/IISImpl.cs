﻿using G.Infrastructure.Contract.Deploy.Model;
using G.Infrastructure.Contract.Deploy.PluginInterfaces;
using G.Infrastructure.Plugin.DeployHost.IIS.DeployModes;
using G.Infrastructure.Plugin.DeployHost.IIS.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Plugin.DeployHost.IIS
{
    public class IISImpl : IDeployHost
    {
        public void Deploy(DeployInfo deployInfo)
        {
            var rootDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var deployDirectory = Path.Combine(rootDirectory, "Download", "Deploy");

            var deployFilePackagePath = Path.Combine(deployDirectory, deployInfo.FilePackageName);

            IDeployWorker deployWorker = null;

            //todo refactoring
            switch (deployInfo.DeployMode)
            {
                case Constant.Enums.DeployMode.WebDeploy:
                    deployWorker = new WebDeployDeployWorker();
                    break;
                case Constant.Enums.DeployMode.Original:
                    deployWorker = new OriginalDeployWorker();
                    break;
                default:
                    break;
            }

            deployWorker.Deploy(deployFilePackagePath, deployInfo.JsonHostProperty);
        }
    }
}
