﻿using AliyunSDK = Aliyun.Acs;
using G.Infrastructure.Contract.Deploy.PluginInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Framework.Caching.Configs;

namespace G.Infrastructure.Plugin.LoadBalance.Aliyun
{
    public class AliyunImpl : ILoadBalance
    {
        private AliyunSDK.Core.DefaultAcsClient _client = null;

        public AliyunImpl()
        {
            var clientProfile = AliyunSDK.Core.Profile.DefaultProfile.GetProfile(
                "cn-hangzhou",
                AppConfigCache.GetAppSettings("AliyunAccessKeyID"),
                AppConfigCache.GetAppSettings("AliyunAccessKeySecret"));

            _client = new AliyunSDK.Core.DefaultAcsClient(clientProfile);
        }

        public async Task<int> SetServerWeight(string loadBalanceValue, string serverIdentity, int weight)
        {
            var request = new AliyunSDK.Slb.Model.V20140515.SetBackendServersRequest();
            request.LoadBalancerId = loadBalanceValue;
            request.BackendServers = $"[{{\"ServerId\":\"{serverIdentity}\",\"Weight\":\"{weight}\"}}]";

            try
            {
                var response = await Task.Run(() =>
                {
                    return _client.GetAcsResponse(request);
                });

                return int.Parse(response.BackendServers.SingleOrDefault(s => s.ServerId == serverIdentity).Weight);
            }
            catch (AliyunSDK.Core.Exceptions.ServerException e)
            {
                throw new Exception(e.ErrorMessage);
            }
            catch (AliyunSDK.Core.Exceptions.ClientException e)
            {
                throw new Exception(e.ErrorMessage);
            }
        }

        public async Task<int> GetServerWeight(string loadBalanceValue, string serverIdentity)
        {
            var request = new AliyunSDK.Slb.Model.V20140515.DescribeLoadBalancerAttributeRequest();
            request.LoadBalancerId = loadBalanceValue;

            try
            {
                var response = await Task.Run(() =>
                {
                    return _client.GetAcsResponse(request);
                });

                return response.BackendServers.Single(bs => bs.ServerId == serverIdentity).Weight.Value;
            }
            catch (AliyunSDK.Core.Exceptions.ServerException e)
            {
                throw new Exception(e.ErrorMessage);
            }
            catch (AliyunSDK.Core.Exceptions.ClientException e)
            {
                throw new Exception(e.ErrorMessage);
            }
        }
    }
}
