using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using G.Infrastructure.Constant.Enums;
using System.Net;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;
using G.Infrastructure.Plugin.CI.Jenkins.Models;
using G.Infrastructure.Contract.Deploy.Model;
using G.Infrastructure.Contract.Deploy.PluginInterfaces;

namespace G.Infrastructure.Plugin.CI.Jenkins
{
    public class JenkinsImpl : ICI
    {
        public async Task<string> BuildJob(string jsonConfig)
        {
            var config = JsonConvert.DeserializeObject<Config>(jsonConfig);

            var url = $"{config.Url}/user/{config.AccountNumber}/my-views/view/All/job/{config.JobName}/build";

            var buffer = new byte[0];
            var request = (HttpWebRequest)WebRequest.Create(url);

            request.Method = HttpMethod.Post.Method;
            request.Headers.Add("Authorization", $"Basic {config.EncodedToken}");

            using (var requestStream = request.GetRequestStream())
            {
                await requestStream.WriteAsync(buffer, 0, buffer.Length);
            }

            var response = await request.GetResponseAsync() as HttpWebResponse;
            var queueItemUrl = response.Headers["Location"];

            request = null;

            int queenItemID = Int32.Parse(queueItemUrl.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries).Last());

            var getJobListUrl = $"{config.Url}/user/{config.AccountNumber}/my-views/view/All/job/{config.JobName}/api/json";
            var result = await HttpGet(getJobListUrl, config.EncodedToken);
            var jobList = JsonConvert.DeserializeObject<JobList>(result);

            var jobNumber = jobList.QueueItem?.ID == queenItemID ? jobList.NextBuildNumber : jobList.Builds.Max(b => b.Number);

            return jobNumber.ToString();
        }

        public Task<ArtifactInfo> GetArtifactInfo(string jsonConfig, string jobNumber,string artifactName)
        {
            var config = JsonConvert.DeserializeObject<Config>(jsonConfig);

            var url = $"{config.Url}/user/{config.AccountNumber}/my-views/view/All/job/{config.JobName}/{jobNumber}/artifact/{config.ArtifactPath}/{artifactName}";

            return Task.FromResult(new ArtifactInfo()
            {
                Name = artifactName,
                Url = url,
                Token = config.EncodedToken
            });
        }        

        public async Task<CIJobStatus> GetStatusByJobNumber(string jsonConfig, string jobNumber)
        {
            var config = JsonConvert.DeserializeObject<Config>(jsonConfig);
            var jobStatus = CIJobStatus.QUEUE_OR_INVALID;
            var jobApiUrl = $"{config.Url}/user/{config.AccountNumber}/my-views/view/All/job/{config.JobName}/{jobNumber}/api/json";

            try
            {
                var result = await HttpGet(jobApiUrl, config.EncodedToken);

                var jobInfo = JsonConvert.DeserializeObject<JobInfo>(result);
                if (jobInfo.Building)
                {
                    jobStatus = CIJobStatus.BUILDING;
                }
                else
                {
                    jobStatus = (CIJobStatus)Enum.Parse(typeof(CIJobStatus), jobInfo.Result);
                }
            }
            catch (WebException ex)
            {
                if (ex.Message.Contains("404"))
                {
                    jobStatus = CIJobStatus.QUEUE_OR_INVALID;
                }
            }

            return jobStatus;
        }

        private async Task<string> HttpGet(string url, string encodedToken)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);

            request.Method = HttpMethod.Get.Method;
            request.Headers.Add("Authorization", $"Basic {encodedToken}");

            using (var response = await request.GetResponseAsync() as HttpWebResponse)
            using (var responseStream = response.GetResponseStream())
            using (var streamReader = new StreamReader(responseStream, Encoding.UTF8))
            {
                var downloadString = await streamReader.ReadToEndAsync();
                streamReader.Close();

                return downloadString;
            }
        }

        public Task<string> GetConsoleUrlByJobNumber(string jsonConfig, string jobNumber)
        {
            var config = JsonConvert.DeserializeObject<Config>(jsonConfig);

            var url = $"{config.Url}/user/{config.AccountNumber}/my-views/view/All/job/{config.JobName}/{jobNumber}/console";

            return Task.FromResult(url);
        }
    }
}
