﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Plugin.CI.Jenkins.Models
{
    public class JobListItem
    {
        [JsonProperty("number")]
        public int Number { get; set; }
    }
}
