﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Plugin.CI.Jenkins.Models
{
    public class JobInfo
    {
        [JsonProperty("building")]
        public bool Building { get; set; }

        [JsonProperty("result")]
        public string Result { get; set; }
    }
}
