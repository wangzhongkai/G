﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Plugin.CI.Jenkins.Models
{
    public class Config
    {
        public string Url { get; set; }

        public string JobName { get; set; }

        public string ArtifactPath { get; set; }

        public string AccountNumber { get; set; }

        public string Token { get; set; }

        public string EncodedToken
        {
            get
            {
                return Convert.ToBase64String(Encoding.UTF8.GetBytes($"{AccountNumber}:{Token}"));
            }
        }
    }
}
