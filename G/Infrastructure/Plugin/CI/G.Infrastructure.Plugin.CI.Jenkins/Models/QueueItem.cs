﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Plugin.CI.Jenkins.Models
{
    public class QueueItem
    {
        [JsonProperty("id")]
        public int ID { get; set; }
    }
}
