﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Plugin.CI.Jenkins.Models
{
    public class JobList
    {
        [JsonProperty("nextBuildNumber")]
        public int NextBuildNumber { get; set; }

        [JsonProperty("builds")]
        public JobListItem[] Builds { get; set; }

        [JsonProperty("queueItem")]
        public QueueItem QueueItem { get; set; }
    }
}
