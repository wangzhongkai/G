﻿using Framework.Caching.Memory;
using G.Infrastructure.Constant.Enums;
using G.Infrastructure.Contract.Deploy.PluginInterfaces;
using G.Infrastructure.Plugin.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Plugin
{
    public class LoadBalanceBuilder
    {
        private static LazyManualMemoryCache<LoadBalanceType, ILoadBalance> _mcCI = new LazyManualMemoryCache<LoadBalanceType, ILoadBalance>();

        public static ILoadBalance Get(LoadBalanceType loadBalanceType)
        {
            return _mcCI.GetOrAdd(loadBalanceType, (lbt) =>
            {
                return BuildHelper.ReflectType<LoadBalanceType, ILoadBalance>(lbt, "bin");
            });
        }
    }
}