﻿using Framework.Caching.Memory;
using G.Infrastructure.Constant.Enums;
using G.Infrastructure.Contract.Deploy.PluginInterfaces;
using G.Infrastructure.Plugin.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Plugin
{
    public class DeployHostBuilder
    {
        private static LazyManualMemoryCache<DeployHostType, IDeployHost> _mc = new LazyManualMemoryCache<DeployHostType, IDeployHost>();

        public static IDeployHost Get(DeployHostType deployHostType)
        {
            return _mc.GetOrAdd(deployHostType, (ct) =>
            {
                return BuildHelper.ReflectType<DeployHostType, IDeployHost>(ct);
            });
        }
    }
}