﻿using Framework.Caching.Memory;
using Framework.Common.EnumOperation;
using G.Infrastructure.Constant.Attributes;
using G.Infrastructure.Constant.Enums;
using G.Infrastructure.Contract.Deploy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Plugin.Helpers
{
    public class BuildHelper
    {
        public static TIPlugin ReflectType<TPluginType, TIPlugin>(TPluginType pluginType, string relativePath = null)
            where TPluginType : struct
            where TIPlugin : class
        {
            var pluginInfoAttribute = EnumHelper.GetEnumAttribute<PluginInfoAttribute>(pluginType);

            var fileDirectory = string.IsNullOrWhiteSpace(relativePath) ? AppDomain.CurrentDomain.BaseDirectory : Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relativePath);
            var filePath = Path.Combine(fileDirectory, $"{pluginInfoAttribute.Namespace}.{pluginType.ToString()}.dll");
            if (!File.Exists(filePath))
            {
                return null;
            }

            Assembly asm = Assembly.Load($"{pluginInfoAttribute.Namespace}.{pluginType.ToString()}");
            var ctType = asm.GetType($"{pluginInfoAttribute.Namespace}.{pluginType.ToString()}.{pluginType.ToString()}Impl");

            if (ctType == null)
                throw new NotImplementedException();

            return Activator.CreateInstance(ctType) as TIPlugin;
        }
    }
}
