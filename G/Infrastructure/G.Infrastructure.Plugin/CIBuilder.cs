﻿using Framework.Caching.Memory;
using G.Infrastructure.Constant.Enums;
using G.Infrastructure.Contract.Deploy.PluginInterfaces;
using G.Infrastructure.Plugin.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Plugin
{
    public class CIBuilder
    {
        private static LazyManualMemoryCache<CIType, ICI> _mcCI = new LazyManualMemoryCache<CIType, ICI>();

        public static ICI Get(CIType ciType)
        {
            return _mcCI.GetOrAdd(ciType, (ct) =>
            {
                return BuildHelper.ReflectType<CIType, ICI>(ct, "bin");
            });
        }
    }
}
