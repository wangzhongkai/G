﻿using Framework.Mapping.Base;
using G.Client.Data.Entities.Base;
using G.Client.Data.Wrapper;
using G.Client.Model.DeployManage;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Data.Pipeline.Database
{
    public class SingleTableEngineer<TEntity, TKey, TListModel, TCreateModel, TEditModel>
        where TEntity : EntityBase<TKey>
        where TListModel : class
        where TCreateModel : class
        where TEditModel : class, IEditViewModel<TKey>
        where TKey : IEquatable<TKey>
    {
        public virtual async Task<List<TListModel>> GetList()
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from entity in GDbContext.GetDbSet<TEntity, TKey>(dbContext).Where(entity => !entity.IsDelete)
                            select entity;

                var mapper = new MapperBase<TListModel, TEntity>();

                return mapper.GetModelList(await query.ToListAsync());
            }
        }

        public virtual async Task<TKey> Create(TCreateModel model)
        {
            using (var dbContext = GDbContext.Create())
            {
                var mapper = new MapperBase<TCreateModel, TEntity>();
                var entity = mapper.GetEntity(model);

                GDbContext.GetDbSet<TEntity, TKey>(dbContext).Add(entity);

                await dbContext.SaveChangesAsync();

                return entity.ID;
            }
        }

        public virtual async Task<TEditModel> GetByID(TKey id)
        {
            using (var dbContext = GDbContext.Create())
            {
                var entity = await GDbContext.GetDbSet<TEntity, TKey>(dbContext).SingleAsync(e => e.ID.Equals(id));
                var mapper = new MapperBase<TEditModel, TEntity>();

                return mapper.GetModel(entity);
            }
        }

        public virtual async Task<TEditModel> Update(TEditModel model)
        {
            using (var dbContext = GDbContext.Create())
            {
                var entity = await GDbContext.GetDbSet<TEntity, TKey>(dbContext).SingleAsync(e => e.ID.Equals(model.ID));

                entity = new MapperBase<TEditModel, TEntity>().GetEntity(model, entity);
                entity.SetUpdateInfo();

                GDbContext.GetDbSet<TEntity, TKey>(dbContext).Attach(entity);
                dbContext.Entry(entity).State = EntityState.Modified;

                await dbContext.SaveChangesAsync();

                return model;
            }
        }
    }
}
