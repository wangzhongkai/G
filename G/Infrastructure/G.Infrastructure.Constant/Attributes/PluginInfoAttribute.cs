﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Constant.Attributes
{
    public class PluginInfoAttribute : Attribute
    {
        public string Namespace { get; set; }
    }
}
