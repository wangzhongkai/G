﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Constant.Attributes
{
    public class DeployFlowAttribute : Attribute
    {
        public int Order { get; set; }

        public string Description { get; set; }

        public bool Required { get; set; }

        public int Value { get; set; }

        public string PartilViewName { get; set; }
    }
}