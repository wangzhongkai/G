﻿using G.Infrastructure.Constant.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Constant.Enums
{
    [Flags]
    public enum DeployFlowType
    {
        [DeployFlow(Description = "创建部署任务", Order = 1, Required = true, Value = 0x1)]
        CreateDeployTask = 0x1,

        /// <summary>
        /// 构建项目暂时为必选
        /// </summary>
        [DeployFlow(Description = "构建项目", Order = 2, Required = true, Value = 0x2, PartilViewName = "_PartialBuildProject")]
        BuildProject = 0x2,

        [DeployFlow(Description = "绑定版本号", Order = 3, Required = true, Value = 0x4)]
        BindingProjectVersion = 0x4,

        [DeployFlow(Description = "获取项目信息", Order = 4, Required = true, Value = 0x8)]
        GetProjectInfo = 0x8,

        [DeployFlow(Description = "通知服务器下载文件包", Order = 5, Required = true, Value = 0x10, PartilViewName = "_PartialNotifyServerDownloadFilePackage")]
        NotifyServerDownloadFilePackage = 0x10,

        [DeployFlow(Description = "安全部署预处理", Order = 6, Required = false, Value = 0x20, PartilViewName = "_PartialSafeDeployPreprocessing")]
        SafeDeployPreprocessing = 0x20,

        [DeployFlow(Description = "部署", Order = 7, Required = true, Value = 0x40, PartilViewName = "_PartialDeploy")]
        Deploy = 0x40,

        [DeployFlow(Description = "触发缓存Url", Order = 8, Required = false, Value = 0x80, PartilViewName = "_PartialTriggerCacheUrl")]
        TriggerCacheUrl = 0x80,

        [DeployFlow(Description = "安全部署收尾", Order = 9, Required = false, Value = 0x100, PartilViewName = "_PartialSafeDeployEnding")]
        SafeDeployEnding = 0x100,
    }
}