using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Constant.Enums
{
    public enum CIJobStatus
    {
        QUEUE_OR_INVALID = 1,

        BUILDING = 2,

        FAILURE = 10,

        SUCCESS = 100,

        UNSTABLE = 101,
    }
}
