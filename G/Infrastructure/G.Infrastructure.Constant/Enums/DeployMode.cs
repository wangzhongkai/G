﻿using Framework.Common.EnumOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Constant.Enums
{
    public enum DeployMode
    {
        [EnumValue("Web Deploy")]
        WebDeploy = 1,

        [EnumValue("原始")]
        Original = 2,
    }
}
