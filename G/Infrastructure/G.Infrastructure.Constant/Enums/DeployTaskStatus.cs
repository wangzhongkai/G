﻿using Framework.Common.EnumOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Constant.Enums
{
    public enum DeployTaskStatus
    {
        [EnumValue(Description = "正在运行")]
        Running = 0,

        [EnumValue(Description = "成功")]
        Success = 1,

        [EnumValue(Description ="失败")]
        Failed = 2,

        [EnumValue(Description = "异步执行")]
        AsyncRun = 3,

        [EnumValue(Description = "副本")]
        Duplicate = 4,
    }
}
