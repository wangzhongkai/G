﻿using G.Infrastructure.Constant.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Constant.Enums
{
    public enum CIType
    {
        /// <summary>
        /// 暂时只支持Jenkins，不允许为空
        /// </summary>
        //None = 0,

        [PluginInfo(Namespace = "G.Infrastructure.Plugin.CI")]
        Jenkins = 1,
    }
}
