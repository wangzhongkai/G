﻿using G.Infrastructure.Constant.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Constant.Enums
{
    public enum DeployHostType
    {
        [PluginInfo(Namespace = "G.Infrastructure.Plugin.DeployHost")]
        IIS = 1,

        [PluginInfo(Namespace = "G.Infrastructure.Plugin.DeployHost")]
        Console = 2
    }
}
