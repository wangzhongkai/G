﻿using Framework.Common.EnumOperation;
using G.Infrastructure.Constant.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Infrastructure.Constant.Enums
{
    public enum LoadBalanceType
    {
        [EnumValue(Description = "无")]
        None = 0,

        [EnumValue(Description = "阿里云")]
        [PluginInfo(Namespace = "G.Infrastructure.Plugin.LoadBalance")]
        Aliyun = 1,
    }
}
