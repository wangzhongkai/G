﻿function http(type, dataType, url, dataObj, successFunc, failedFunc) {
    $.ajax({
        type: type,
        dataType: dataType,
        url: url,
        data: dataObj,
        traditional: true,
        success: function (data) {
                successFunc(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (failedFunc === null || failedFunc === undefined) {
                alert(XMLHttpRequest.responseText);
            } else {
                failedFunc(XMLHttpRequest, textStatus, errorThrown)
            }
        }
    });
}