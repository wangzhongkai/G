﻿var appGlobalModal = new Vue({
    el: '#globalModal',
    data: {
        modalData: {},
    },
    methods: {
        set: function (title, body) {
            this.modalData = {
                Title: title,
                Body: body,
            };

            $('#globalModal').modal('show');
        }
    }
});