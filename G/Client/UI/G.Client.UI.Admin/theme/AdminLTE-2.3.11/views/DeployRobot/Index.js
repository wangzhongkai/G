﻿function pageLoaded() {
    http('get', 'json', '/DeployRobot/GetViewModel/',
        {
            deployEnvironmentID: deployEnvironmentID,
            deployTaskID: deployTaskID,
            optionalDeployServerID: optionalDeployServerID
        },
        function (data) {
            deployRobotApp.deployRobot = data;

            deployRobotApp.checkCIJobStatus();
        },
        function (XMLHttpRequest, textStatus, errorThrown) {
            failedFunc(XMLHttpRequest, textStatus, errorThrown, null);
        });
}

function checkCIJobStatusLogic() {
    http('get', 'text', '/DeployRobot/GetCIJobStatusDescription/', { deployProjectVersionID: deployRobotApp.deployRobot.DeployProjectVersion.ID },
        function (data) {
            deployRobotApp.ciJobStatus = data;

            switch (data) {
                case 'SUCCESS':
                    $('#ciJobStatusLoading').hide();
                    deployRobotApp.deployStatus = deployStatusEnum.buildSuccess;
                    break;
                case 'QUEUE_OR_INVALID':
                case 'BUILDING':
                    $('#ciJobStatusLoading').show();
                    setTimeout(function () {
                        deployRobotApp.checkCIJobStatus();
                    }, 1000);
                    break;
                default:
                    $('#ciJobStatusLoading').hide();
                    deployRobotApp.ciJobStatus = data;
            }
        },
        function (XMLHttpRequest, textStatus, errorThrown) {
            failedFunc(XMLHttpRequest, textStatus, errorThrown, null);
        });
}

function oneKeyDeployLogic() {
    getDeployInfo();
}

function getDeployInfo() {
    deployRobotApp.deployStatus = deployStatusEnum.getDeployInfo;

    http('get', 'json', '/DeployRobot/GetDeployProject/', { deployProjectID: deployRobotApp.deployRobot.DeployEnvironment.DeployProjectID },
        function (data) {
            deployRobotApp.deployRobotDeployProject = data;

            deployRobotApp.deployStatus = deployStatusEnum.deploying;

            deploy(0);
        },
        function (XMLHttpRequest, textStatus, errorThrown) {
            failedFunc(XMLHttpRequest, textStatus, errorThrown, function () {
                getDeployInfo();
            });
        });
}

function deploy(index) {
    initDeploying(index);

    downloadDeployFilePackage();
}

function initDeploying(index) {
    deployRobotApp.deployStatus = deployStatusEnum.deploying;

    deployRobotApp.$set(deployRobotApp.deploying, 'totalProgress', 0);
    deployRobotApp.$set(deployRobotApp.deploying, 'index', 0);
    deployRobotApp.$set(deployRobotApp.deploying, 'autoRetryDeployTime', 0);
    deployRobotApp.$set(deployRobotApp.deploying, 'deployingServer', deployRobotApp.deployRobot.DeployEnvironment.DeployServerList[index]);

    deployRobotApp.$set(deployRobotApp.deploying.deployingServer, 'DeployProgress', 0);
    deployRobotApp.$set(deployRobotApp.deploying.deployingServer, 'DeployDescription', '');
    deployRobotApp.$set(deployRobotApp.deploying.deployingServer, 'DeployStatusCssClass', null);

    deployRobotApp.deploying.index = index;
    deployRobotApp.deploying.deployingServer = deployRobotApp.deployRobot.DeployEnvironment.DeployServerList[deployRobotApp.deploying.index];
    deployRobotApp.deploying.autoRetryDeployTime = deployRobotApp.deployRobotDeployProject.DeployProject.AutoRetryDeployTime;
    deployRobotApp.deploying.totalProgress = deployRobotApp.deploying.index / deployRobotApp.deployRobot.DeployEnvironment.DeployServerList.length * 100;
}

function downloadDeployFilePackage() {
    deployRobotApp.deploying.deployingServer.DeployDescription = "下载部署文件包";

    http('post', 'text', '/DeployRobot/NotifyServerDownloadFilePackage/', {
        deployServerIP: deployRobotApp.deploying.deployingServer.IPAddress,
        ciID: deployRobotApp.deployRobotDeployProject.DeployProject.DeployFlowProperty.DeployCIID,
        ciJobIdentity: deployRobotApp.deployRobot.DeployProjectVersion.CIJobIdentity,
        ciArtifactName: deployRobotApp.deployRobotDeployProject.DeployProject.DeployFlowProperty.DeployCIArtifactName
    },
        function (data) {
            deployRobotApp.deploying.deployingServer.DeployProgress = 20;
            decideSetServerWeight(0, 30, decideCheckWindowsPerformanceMonitor);
        },
        function (XMLHttpRequest, textStatus, errorThrown) {
            failedFunc(XMLHttpRequest, textStatus, errorThrown, function () {
                downloadDeployFilePackage();
            });
        });
}

function decideCheckWindowsPerformanceMonitor() {
    if (deployRobotApp.deployRobotDeployProject.WindowsPerformanceMonitor !== null) {
        deployRobotApp.deploying.deployingServer.DeployDescription = "检查Windows性能监视器";
        checkWindowsPerformanceMonitor(deployRobotApp.deployRobotDeployProject.DeployProject.DeployFlowProperty.WindowsPerformanceMonitorCheckMaximumLatency);
    } else {
        applicationDeploy();
    }
}

function checkWindowsPerformanceMonitor(maximumLatency) {
    if (maximumLatency <= 0) {
        deployRobotApp.deploying.deployingServer.DeployProgress = 50;
        applicationDeploy();
    } else {
        maximumLatency--;

        var machineName = deployRobotApp.deployRobotDeployProject.WindowsPerformanceMonitor.MachineName;
        if (machineName === null || machineName === "") {
            machineName = deployRobotApp.deploying.deployingServer.IPAddress;
        }

        http('get', 'text', '/DeployRobot/GetWindowsPerformanceMonitorValue/',
            {
                categoryName: deployRobotApp.deployRobotDeployProject.WindowsPerformanceMonitor.CategoryName,
                counterName: deployRobotApp.deployRobotDeployProject.WindowsPerformanceMonitor.CounterName,
                instanceName: deployRobotApp.deployRobotDeployProject.WindowsPerformanceMonitor.InstanceName,
                machineName: machineName
            },
            function (data) {
                var windowsPerformanceMonitorValue = Number(data);
                deployRobotApp.deploying.deployingServer.DeployDescription = "休眠[" + maximumLatency + "]秒或检查值[" + windowsPerformanceMonitorValue + "]<=[" + deployRobotApp.deployRobotDeployProject.WindowsPerformanceMonitor.ThresholdValue + "]";

                if (windowsPerformanceMonitorValue <= deployRobotApp.deployRobotDeployProject.WindowsPerformanceMonitor.ThresholdValue) {
                    deployRobotApp.deploying.deployingServer.DeployProgress = 50;
                    applicationDeploy();
                } else {
                    setTimeout(function () {
                        checkWindowsPerformanceMonitor(maximumLatency);
                    }, 1000);
                }
            },
            function (XMLHttpRequest, textStatus, errorThrown) {
                failedFunc(XMLHttpRequest, textStatus, errorThrown, function () {
                    checkWindowsPerformanceMonitor(maximumLatency);
                });
            });
    }
}

function decideSetServerWeight(weight, deployProgress, callBack) {
    if (deployRobotApp.deployRobotDeployProject.DeployCI !== null) {
        setServerWeight(weight, deployProgress, callBack);
    } else {
        callBack();
    }
}

function setServerWeight(weight, deployProgress, callBack) {
    deployRobotApp.deploying.deployingServer.DeployDescription = "设置服务器权重:[" + weight + "]";

    http('post', 'text', '/DeployRobot/SetLoadBalanceServerWeight/',
        {
            loadBalanceType: deployRobotApp.deployRobotDeployProject.DeployProject.DeployFlowProperty.LoadBalanceType,
            loadBalanceValue: deployRobotApp.deployRobotDeployProject.DeployProject.DeployFlowProperty.LoadBalanceValue,
            serverIdentity: deployRobotApp.deploying.deployingServer.ServerIdentity,
            weight: weight
        },
        function (data) {
            deployRobotApp.deploying.deployingServer.DeployProgress = deployProgress;

            callBack();
        },
        function (XMLHttpRequest, textStatus, errorThrown) {
            failedFunc(XMLHttpRequest, textStatus, errorThrown, function () {
                setServerWeight(weight, deployProgress, callBack);
            });
        });
}

function applicationDeploy() {
    deployRobotApp.deploying.deployingServer.DeployDescription = "应用程序部署";

    http('post', 'text', '/DeployRobot/ApplicationDeploy/',
        {
            deployServerIP: deployRobotApp.deploying.deployingServer.IPAddress,
            deployProjectType: deployRobotApp.deployRobotDeployProject.DeployProject.Type,
            deployProjectHost: deployRobotApp.deployRobotDeployProject.DeployProject.DeployFlowProperty.Host,
            deployMode: deployRobotApp.deployRobotDeployProject.DeployProject.DeployFlowProperty.DeployMode,
            jsonHostProperty: deployRobotApp.deployRobotDeployProject.DeployProject.DeployFlowProperty.JsonHostProperty,
            ciID: deployRobotApp.deployRobotDeployProject.DeployProject.DeployFlowProperty.DeployCIID,
            ciJobIdentity: deployRobotApp.deployRobot.DeployProjectVersion.CIJobIdentity,
            ciArtifactName: deployRobotApp.deployRobotDeployProject.DeployProject.DeployFlowProperty.DeployCIArtifactName
        },
        function (data) {
            deployRobotApp.deploying.deployingServer.DeployProgress = 70;

            decideTriggerCache();
        },
        function (XMLHttpRequest, textStatus, errorThrown) {
            failedFunc(XMLHttpRequest, textStatus, errorThrown, function () {
                applicationDeploy();
            });
        });
}

function decideTriggerCache() {
    if (deployRobotApp.deployRobotDeployProject.DeployProject.DeployFlowProperty.TriggerCacheUrl !== null &&
        deployRobotApp.deployRobotDeployProject.DeployProject.DeployFlowProperty.TriggerCacheUrl !== "") {
        triggerCache();
    } else {
        decideSetServerWeight(100, 100, setSuccess);
    }
}

function triggerCache() {
    deployRobotApp.deploying.deployingServer.DeployDescription = "触发缓存";

    http('post', 'text', '/DeployRobot/TriggerCache/',
        {
            deployServerIP: deployRobotApp.deploying.deployingServer.IPAddress,
            triggerCacheUrl: deployRobotApp.deployRobotDeployProject.DeployProject.DeployFlowProperty.TriggerCacheUrl
        },
        function (data) {
            deployRobotApp.deploying.deployingServer.DeployProgress = 90;

            decideSetServerWeight(100, 100, setSuccess);
        },
        function (XMLHttpRequest, textStatus, errorThrown) {
            failedFunc(XMLHttpRequest, textStatus, errorThrown, function () {
                triggerCache();
            });
        });
}

function setSuccess() {
    deployRobotApp.deploying.deployingServer.DeployProgress = 100;
    deployRobotApp.deploying.deployingServer.DeployDescription = "部署完成";
    deployRobotApp.deploying.deployingServer.DeployStatusCssClass = "bg-green";

    if (deployRobotApp.deploying.index + 1 < deployRobotApp.deployRobot.DeployEnvironment.DeployServerList.length) {
        deploy(deployRobotApp.deploying.index + 1);
    } else {
        deployRobotApp.deploying.totalProgress = 100;
        deployRobotApp.deployStatus = deployStatusEnum.success;
    }
}

function failedFunc(XMLHttpRequest, textStatus, errorThrown, retryFunc) {
    if (deployRobotApp.deploying.autoRetryDeployTime > 0) {
        deployRobotApp.deploying.autoRetryDeployTime--;

        if (retryFunc !== null) {
            retryFunc();

            deployRobotApp.deployStatus = deployStatusEnum.autoRetryDeploying;
        }
    } else {
        deployRobotApp.deploying.exceptionMessage = XMLHttpRequest.responseText;
        deployRobotApp.deployStatus = deployStatusEnum.failed;
        deployRobotApp.deploying.deployingServer.DeployStatusCssClass = "bg-red";
    }
}