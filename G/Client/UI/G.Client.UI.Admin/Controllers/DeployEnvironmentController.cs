﻿using G.Client.Business.DeployManage;
using G.Client.Model.DeployManage.DeployEnvironmentModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace G.Client.UI.Admin.Controllers
{
    [Authorize]
    public class DeployEnvironmentController : Controller
    {
        // GET: DeployEnvironmentManage
        public async Task<ActionResult> Index()
        {
            var lst = await new DeployEnvironmentBusiness().GetMangeViewList();

            return View(lst);
        }

        public async Task<ActionResult> Create()
        {
            await InitOptionsData();

            return View();
        }

        private async Task InitOptionsData()
        {
            ViewBag.DeployProjectList = await new DeployProjectBusiness().GetList();
            ViewBag.DeployEnvironmentTagList = await new DeployEnvironmentTagBusiness().GetList();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateDeployEnvironmentViewModel model)
        {
            ViewBag.IsPostBack = true;
            await InitOptionsData();

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var id = await new DeployEnvironmentBusiness().Create(model);

            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var model = await new DeployEnvironmentBusiness().GetByID(id);

            await InitOptionsData();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditDeployEnvironmentViewModel model)
        {
            ViewBag.IsPostBack = true;
            await InitOptionsData();

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            await new DeployEnvironmentBusiness().Update(model);

            return View(model);
        }
    }
}