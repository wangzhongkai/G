﻿using G.Client.Business.DeployManage;
using G.Client.Model.DeployManage.WindowsPerformanceMonitorModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace G.Client.UI.Admin.Controllers
{
    [Authorize]
    public class WindowsPerformanceMonitorController : Controller
    {
        // GET: DeploySafetyCheck
        public async Task<ActionResult> Index()
        {
            var lst = await new WindowsPerformanceMonitorBusiness().GetManageViewList();

            return View(lst);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateWindowsPerformanceMonitorViewModel model)
        {
            ViewBag.IsPostBack = true;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var id = await new WindowsPerformanceMonitorBusiness().Create(model);

            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var model = await new WindowsPerformanceMonitorBusiness().GetByID(id);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditWindowsPerformanceMonitorViewModel model)
        {
            ViewBag.IsPostBack = true;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            await new WindowsPerformanceMonitorBusiness().Update(model);

            return View(model);
        }

        public async Task<JsonResult> GetByID(int id)
        {
            return new GJsonResult()
            {
                Data = await new WindowsPerformanceMonitorBusiness().GetByID(id),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
            };
        }
    }
}