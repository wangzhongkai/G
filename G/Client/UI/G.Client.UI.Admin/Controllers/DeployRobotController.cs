﻿using G.Client.Business.DeployManage;
using G.Infrastructure.Constant.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace G.Client.UI.Admin.Controllers
{
    [Authorize]
    public class DeployRobotController : Controller
    {
        public ActionResult Index(int deployEnvironmentID, int deployTaskID, int[] optionalDeployServerID)
        {
            return View();
        }

        public async Task<ActionResult> GetCIJobStatusDescription(int deployProjectVersionID)
        {
            var ciJobStatus = await new DeployRobotBusiness().GetCIJobStatus(deployProjectVersionID);

            return Content(ciJobStatus.ToString());
        }

        public async Task<JsonResult> GetViewModel(int deployEnvironmentID, int deployTaskID, int[] optionalDeployServerID)
        {
            var deployRobot = await new DeployEnvironmentBusiness().GetViewByID(deployEnvironmentID, deployTaskID, optionalDeployServerID);

            return new GJsonResult()
            {
                Data = deployRobot,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public async Task<JsonResult> GetDeployProject(int deployProjectID)
        {
            var deployProject = await new DeployProjectBusiness().GetDeployProjectView(deployProjectID);

            return new GJsonResult()
            {
                Data = deployProject,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public async Task<ContentResult> NotifyServerDownloadFilePackage(string deployServerIP, int ciID, string ciJobIdentity, string ciArtifactName)
        {
            await new DeployRobotBusiness().NotifyServerDownloadFilePackage(deployServerIP, ciID, ciJobIdentity, ciArtifactName);

            return Content("success");
        }

        [HttpPost]
        public async Task<ContentResult> SetLoadBalanceServerWeight(LoadBalanceType loadBalanceType, string loadBalanceValue, string serverIdentity, int weight)
        {
            var currentWeight = await new DeployRobotBusiness().SetLoadBalanceServerWeightAsync(loadBalanceType, loadBalanceValue, serverIdentity, weight);

            return Content(currentWeight.ToString());
        }

        public ContentResult GetWindowsPerformanceMonitorValue(string categoryName, string counterName, string instanceName, string machineName)
        {
            var value = new DeployRobotBusiness().GetWindowsPerformanceMonitorValue(categoryName, counterName, instanceName, machineName);

            return Content(value.ToString());
        }

        [HttpPost]
        public async Task<ContentResult> ApplicationDeploy(string deployServerIP, DeployProjectType deployProjectType, DeployHostType deployProjectHost, DeployMode deployMode, string jsonHostProperty, int ciID, string ciJobIdentity, string ciArtifactName)
        {
            await new DeployRobotBusiness().ApplicationDeploy(deployServerIP, deployProjectType, deployProjectHost, deployMode, jsonHostProperty, ciID, ciJobIdentity, ciArtifactName);

            return Content("success");
        }

        [HttpPost]
        public async Task<ContentResult> TriggerCache(string deployServerIP, string triggerCacheUrl)
        {
            var content = await new DeployRobotBusiness().TriggerCache(deployServerIP, triggerCacheUrl);

            return Content(content);
        }
    }
}