﻿using G.Client.Business.DeployManage;
using G.Client.Model.DeployManage.DeployManageModels;
using G.Client.Model.DeployManage.DeployProjectGroupModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace G.Client.UI.Admin.Controllers
{
    [Authorize]
    public class DeployManageController : Controller
    {
        // GET: DeployManage
        public async Task<ActionResult> Index()
        {
            var lst = await new DeployProjectGroupBusiness().GetManageViewList();

            return View(lst);
        }

        public async Task<ActionResult> Project(int id)
        {
            var deployManageProject = await new DeployManageBusiness().GetDeployProjectByID(id);
            ViewBag.DeployCIList = await new DeployCIBusiness().GetList();
            ViewBag.WindowsPerformanceMonitorList = await new WindowsPerformanceMonitorBusiness().GetList();

            return View(deployManageProject);
        }

        public async Task<ActionResult> CreateDeployTask(int id)
        {
            await InitCreateViewBag(id);

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateDeployTask(CreateDeployTaskViewModel model)
        {
            ViewBag.IsPostBack = true;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var id = await new DeployManageBusiness().CreateDeployTask(model);

            if (model.DeployEnvironmentID == 0)
            {
                await InitCreateViewBag(model.DeployProjectID);

                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "DeployRobot", new { deployEnvironmentID = model.DeployEnvironmentID, deployTaskID = id });
            }
        }

        public async Task<JsonResult> GetDeployManageTaskList(int deployProjectID)
        {
            var lst = await new DeployManageBusiness().GetDeployManageTaskList(deployProjectID);

            return new GJsonResult
            {
                Data = lst.OrderByDescending(dmt => dmt.DeployTask.CreateTime).ToList(),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public async Task<JsonResult> GetEnvironmentListByDeployTaskID(int deployTaskID)
        {
            return new GJsonResult()
            {
                Data = await new DeployManageBusiness().GetEnvironmentListByDeployTaskID(deployTaskID),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
            };
        }

        public async Task<JsonResult> GetProjectVersionListByDeployProjectID(int deployProjectID)
        {
            return new GJsonResult()
            {
                Data = await new DeployManageBusiness().GetProjectVersionListByDeployProjectID(deployProjectID),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
            };
        }

        [HttpPost]
        public async Task<JsonResult> CreateDeployTaskByDeployProjectID(int deployProjectVersionID)
        {
            return new GJsonResult()
            {
                Data = new { ID = await new DeployManageBusiness().CreateDeployTaskByDeployProjectID(deployProjectVersionID) },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
            };
        }

        public async Task<ContentResult> GetConsoleUrlByJobNumber(int deployProjectVersionID)
        {
            return Content(await new DeployManageBusiness().GetConsoleUrlByJobNumber(deployProjectVersionID));
        }

        public async Task<JsonResult> GetEnvironmentListByDeployProjectID(int deployProjectID)
        {
            return new GJsonResult()
            {
                Data = await new DeployEnvironmentBusiness().GetViewListByDeployProjectID(deployProjectID),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
            };
        }

        public async Task<ContentResult> GetLoadBalanceServerWeight(int deployProjectID, int deployServerID)
        {
            var currentWeight = await new DeployManageBusiness().GetLoadBalanceServerWeight(deployProjectID, deployServerID);

            return Content(currentWeight.ToString());
        }

        [HttpPost]
        public async Task<ContentResult> SetLoadBalanceServerWeight(int deployProjectID, int deployServerID, int weight)
        {
            var currentWeight = await new DeployManageBusiness().SetLoadBalanceServerWeight(deployProjectID, deployServerID, weight);

            return Content(currentWeight.ToString());
        }

        private async Task InitCreateViewBag(int deployProjectID)
        {
            ViewBag.DeployProjectID = deployProjectID;
            ViewBag.RecommendVersionNumber = await new DeployManageBusiness().GetRecommendVersionNumber(deployProjectID);
            ViewBag.EnvironmentList = await new DeployEnvironmentBusiness().GetViewListByDeployProjectID(deployProjectID);
            ViewBag.CopyDeployProjectVersionList = await new DeployManageBusiness().GetCopyDeployProjectVersionList(deployProjectID);
        }
    }
}