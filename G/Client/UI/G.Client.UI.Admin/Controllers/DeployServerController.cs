﻿using G.Client.Business.DeployManage;
using G.Client.Model.DeployManage.DeployServerModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace G.Client.UI.Admin.Controllers
{
    [Authorize]
    public class DeployServerController : Controller
    {
        // GET: DeployManage
        public async Task<ActionResult> Index()
        {
            var deployServerList = await new DeployServerBusiness().GetList();

            return View(deployServerList);
        }

        public async Task<ActionResult> Create()
        {
            ViewBag.DeployEnvironmentList = await new DeployEnvironmentBusiness().GetMangeViewList();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateDeployServerViewModel model)
        {
            ViewBag.IsPostBack = true;
            ViewBag.DeployEnvironmentList = await new DeployEnvironmentBusiness().GetMangeViewList();

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var id = await new DeployServerBusiness().Create(model);

            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var model = await new DeployServerBusiness().GetByID(id);

            ViewBag.DeployEnvironmentList = await new DeployEnvironmentBusiness().GetMangeViewList();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditDeployServerViewModel model)
        {
            ViewBag.IsPostBack = true;
            ViewBag.DeployEnvironmentList = await new DeployEnvironmentBusiness().GetMangeViewList();

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            await new DeployServerBusiness().Update(model);

            return View(model);
        }

        public async Task<JsonResult> GetDeployServerListByDeployEnvironmentID(int deployEnvironmentID)
        {
            return new GJsonResult()
            {
                Data = await new DeployServerBusiness().GetDeployServerListByDeployEnvironmentID(deployEnvironmentID),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}