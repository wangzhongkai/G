﻿using G.Client.Business.DeployManage;
using G.Client.Model.DeployManage.DeployCIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace G.Client.UI.Admin.Controllers
{
    [Authorize]
    public class DeployCIController : Controller
    {
        // GET: DeployCI
        public async Task<ActionResult> Index()
        {
            var lst = await new DeployCIBusiness().GetList();

            return View(lst);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateDeployCIViewModel model)
        {
            ViewBag.IsPostBack = true;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var id = await new DeployCIBusiness().Create(model);

            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var model = await new DeployCIBusiness().GetByID(id);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditDeployCIViewModel model)
        {
            ViewBag.IsPostBack = true;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            await new DeployCIBusiness().Update(model);

            return View(model);
        }

        public async Task<JsonResult> GetList()
        {
            var lst = await new DeployCIBusiness().GetList();

            return new GJsonResult()
            {
                Data = lst,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public async Task<JsonResult> GetByID(int id)
        {
            return new GJsonResult()
            {
                Data = await new DeployCIBusiness().GetByID(id),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}