﻿using G.Client.Business.DeployManage;
using G.Client.Model.DeployManage.DeployProjectGroupModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace G.Client.UI.Admin.Controllers
{
    [Authorize]
    public class DeployProjectGroupController : Controller
    {
        // GET: DeployProjectGroup
        public async Task<ActionResult> Index(int? id = null)
        {
            List<DeployProjectGroupManageViewModel> lst = new List<DeployProjectGroupManageViewModel>();
            if (!id.HasValue)
            {
                lst = await new DeployProjectGroupBusiness().GetManageViewList();
            }
            else
            {
                lst.Add(await new DeployProjectGroupBusiness().GetManageViewByID(id.Value));
            }

            return View(lst);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateDeployProjectGroupViewModel model)
        {
            ViewBag.IsPostBack = true;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var id = await new DeployProjectGroupBusiness().Create(model);

            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var model = await new DeployProjectGroupBusiness().GetByID(id);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditDeployProjectGroupViewModel model)
        {
            ViewBag.IsPostBack = true;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            await new DeployProjectGroupBusiness().Update(model);

            return View(model);
        }
    }
}