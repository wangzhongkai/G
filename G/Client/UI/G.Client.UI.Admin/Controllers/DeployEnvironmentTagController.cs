﻿using G.Client.Business.DeployManage;
using G.Client.Model.DeployManage.DeployEnvironmentTagModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace G.Client.UI.Admin.Controllers
{
    [Authorize]
    public class DeployEnvironmentTagController : Controller
    {
        // GET: DeployEnvironmentTag
        public async Task<ActionResult> Index()
        {
            var lst = await new DeployEnvironmentTagBusiness().GetList();

            return View(lst);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateDeployEnvironmentTagViewModel model)
        {
            ViewBag.IsPostBack = true;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var id = await new DeployEnvironmentTagBusiness().Create(model);

            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var model = await new DeployEnvironmentTagBusiness().GetByID(id);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditDeployEnvironmentTagViewModel model)
        {
            ViewBag.IsPostBack = true;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            await new DeployEnvironmentTagBusiness().Update(model);

            return View(model);
        }
    }
}