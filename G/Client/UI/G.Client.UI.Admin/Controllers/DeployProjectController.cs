﻿using G.Client.Business.DeployManage;
using G.Client.Model.DeployManage.DeployProjectModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace G.Client.UI.Admin.Controllers
{
    [Authorize]
    public class DeployProjectController : Controller
    {
        // GET: DeployProject
        public async Task<ActionResult> Index()
        {
            List<DeployProjectManageViewModel> lst = await new DeployProjectBusiness().GetManageViewList();

            return View(lst);
        }

        public async Task<ActionResult> Create()
        {
            await InitViewBag();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateDeployProjectViewModel model)
        {
            ViewBag.IsPostBack = true;
            await InitViewBag();

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var id = await new DeployProjectBusiness().Create(model);

            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var model = await new DeployProjectBusiness().GetByID(id);

            await InitViewBag();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditDeployProjectViewModel model)
        {
            ViewBag.IsPostBack = true;
            await InitViewBag();

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            await new DeployProjectBusiness().Update(model);

            return View(model);
        }

        private async Task InitViewBag()
        {
            ViewBag.DeployProjectGroupList = await new DeployProjectGroupBusiness().GetList();
            ViewBag.DeployCIList = await new DeployCIBusiness().GetList();
            ViewBag.WindowsPerformanceMonitorList = await new WindowsPerformanceMonitorBusiness().GetList();
        }
    }
}