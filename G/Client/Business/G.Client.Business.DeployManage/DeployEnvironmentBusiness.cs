﻿using Framework.Mapping.Base;
using G.Client.Data.Entities.DeployManage;
using G.Client.Data.Wrapper;
using G.Infrastructure.Data.Pipeline.Database;
using G.Client.Model.DeployManage.DeployEnvironmentModels;
using G.Client.Model.DeployManage.DeployRobotModels;
using G.Client.Model.DeployManage.DeployServerModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using G.Infrastructure.Constant.Enums;

namespace G.Client.Business.DeployManage
{
    public class DeployEnvironmentBusiness : SingleTableEngineer<DeployEnvironment, int, DeployEnvironmentViewModel, CreateDeployEnvironmentViewModel, EditDeployEnvironmentViewModel>
    {
        public async Task<List<DeployEnvironmentManageViewModel>> GetMangeViewList()
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from deployEnvironmentTag in dbContext.DeployEnvironmentTag.Where(det => !det.IsDelete)
                            join deployEnvironment in dbContext.DeployEnvironment.Where(de => !de.IsDelete) on deployEnvironmentTag.ID equals deployEnvironment.DeployEnvironmentTagID into deployEnvironments
                            from deployEnvironment in deployEnvironments.DefaultIfEmpty()
                            group new { DeployEnvironment = deployEnvironment } by new { deployEnvironmentTag.ID, deployEnvironmentTag.Name } into deployEnvironmentTags
                            select new DeployEnvironmentManageViewModel
                            {
                                DeployEnvironmentTagID = deployEnvironmentTags.Key.ID,
                                DeployEnvironmentTagName = deployEnvironmentTags.Key.Name,
                                DeployEnvironmentManageItemViewModelList = deployEnvironmentTags.Where(det => det.DeployEnvironment != null).Select(det =>
                                    new DeployEnvironmentManageItemViewModel()
                                    {
                                        ID = det.DeployEnvironment.ID,
                                        DeployEnvironmentTagID = det.DeployEnvironment.DeployEnvironmentTagID,
                                        DeployProjectID = det.DeployEnvironment.DeployProjectID,
                                        DeployProjectName = dbContext.DeployProject.FirstOrDefault(dp => dp.ID == det.DeployEnvironment.DeployProjectID).Name,
                                        Name = det.DeployEnvironment.Name,
                                        DeployServerList = (from deployEnvironmentServer in dbContext.DeployEnvironmentServer.Where(des => !des.IsDelete && des.DeployEnvironmentID == det.DeployEnvironment.ID)
                                                            join deployServer in dbContext.DeployServer.Where(ds => !ds.IsDelete) on deployEnvironmentServer.DeployServerID equals deployServer.ID
                                                            select new DeployServerViewModel()
                                                            {
                                                                ID = deployServer.ID,
                                                                Name = deployServer.Name,
                                                                IPAddress = deployServer.IPAddress,
                                                                ServerIdentity = deployServer.ServerIdentity,
                                                                Status = deployServer.Status,
                                                            }).OrderBy(ds => ds.Name).ToList()
                                    }
                                ).ToList()
                            };

                return await query.ToListAsync();
            }
        }

        public async Task<DeployRobotViewModel> GetViewByID(int deployEnvironmentID, int deployTaskID, int[] optionalDeployServerID)
        {
            var deployProjectVersion = await new DeployRobotBusiness().GetDeployProjectVersionByDeployTaskID(deployTaskID);
            deployProjectVersion.Description = deployProjectVersion.Description.Substring(0, deployProjectVersion.Description.Length > 20 ? 20 : deployProjectVersion.Description.Length);

            using (var dbContext = GDbContext.Create())
            {
                Expression<Func<DeployServer, bool>> deployServerExp = (ds) => !ds.IsDelete && ds.Status == DeployServerStatus.Online;
                deployServerExp = optionalDeployServerID != null && optionalDeployServerID.Length > 0 ? deployServerExp.And(ds => optionalDeployServerID.Contains(ds.ID)) : deployServerExp;

                var query = from deployEnvironment in dbContext.DeployEnvironment.Where(de => de.ID == deployEnvironmentID)
                            select new DeployEnvironmentManageItemViewModel()
                            {
                                ID = deployEnvironment.ID,
                                DeployEnvironmentTagID = deployEnvironment.DeployEnvironmentTagID,
                                DeployProjectID = deployEnvironment.DeployProjectID,
                                DeployProjectName = dbContext.DeployProject.FirstOrDefault(dp => dp.ID == deployEnvironment.DeployProjectID).Name,
                                Name = deployEnvironment.Name,
                                DeployServerList = (from deployEnvironmentServer in dbContext.DeployEnvironmentServer.Where(des => !des.IsDelete && des.DeployEnvironmentID == deployEnvironment.ID)
                                                    join deployServer in dbContext.DeployServer.Where(deployServerExp) on deployEnvironmentServer.DeployServerID equals deployServer.ID
                                                    select new DeployServerViewModel()
                                                    {
                                                        ID = deployServer.ID,
                                                        Name = deployServer.Name,
                                                        IPAddress = deployServer.IPAddress,
                                                        ServerIdentity = deployServer.ServerIdentity,
                                                        Status = deployServer.Status
                                                    }).OrderBy(ds => ds.Name).ToList()
                            };

                return new DeployRobotViewModel()
                {
                    DeployEnvironment = await query.SingleAsync(),
                    DeployProjectVersion = deployProjectVersion
                };
            }
        }

        public async Task<List<DeployEnvironmentViewModel>> GetViewListByDeployProjectID(int deployProjectID)
        {
            using (var dbContext = GDbContext.Create())
            {
                return await dbContext.DeployEnvironment.Where(de => !de.IsDelete && de.DeployProjectID == deployProjectID)
                    .Select(de => new DeployEnvironmentViewModel()
                    {
                        ID = de.ID,
                        Name = de.Name,
                        DeployProjectID = de.DeployProjectID,
                        DeployEnvironmentTagID = de.DeployEnvironmentTagID
                    }).ToListAsync();
            }
        }
    }
}
