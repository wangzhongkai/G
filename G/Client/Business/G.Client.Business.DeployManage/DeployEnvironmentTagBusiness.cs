﻿using G.Client.Data.Entities.DeployManage;
using G.Client.Data.Wrapper;
using G.Infrastructure.Data.Pipeline.Database;
using G.Client.Model.DeployManage;
using G.Client.Model.DeployManage.DeployEnvironmentTagModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Business.DeployManage
{
    public class DeployEnvironmentTagBusiness : SingleTableEngineer<DeployEnvironmentTag, int, DeployEnvironmentTagViewModel, CreateDeployEnvironmentTagViewModel, EditDeployEnvironmentTagViewModel>
    {
        public async override Task<List<DeployEnvironmentTagViewModel>> GetList()
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from entity in dbContext.DeployEnvironmentTag.Where(entity => !entity.IsDelete)
                            join user in dbContext.User.Where(user => !user.IsDelete) on entity.UpdateUserID equals user.ID
                            select new DeployEnvironmentTagViewModel()
                            {
                                ID = entity.ID,
                                Name = entity.Name,
                                UpdateUserID = entity.UpdateUserID,
                                UpdateUserName = user.Name,
                                UpdateTime = entity.UpdateTime
                            };

                return await query.ToListAsync();
            }
        }
    }
}
