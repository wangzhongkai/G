﻿using Framework.Mapping.Base;
using G.Client.Business.DeployManage.DeploySlaveService;
using G.Client.Data.Entities.DeployManage;
using G.Client.Data.Wrapper;
using G.Infrastructure.Constant.Enums;
using G.Infrastructure.Contract.Deploy.Model;
using G.Infrastructure.Plugin;
using G.Client.Model.DeployManage.DeployProjectVersionModels;
using G.Client.Model.DeployManage.DeployRobotModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using G.Client.Business.DeployManage.Helpers;
using System.Net.Http;

namespace G.Client.Business.DeployManage
{
    public class DeployRobotBusiness
    {
        public async Task<CIJobStatus> GetCIJobStatus(int deployProjectVersionID)
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from dpv in dbContext.DeployProjectVersion.Where(dpv => dpv.ID == deployProjectVersionID)
                            join dfp in dbContext.DeployFlowProperty on dpv.DeployProjectID equals dfp.DeployProjectID
                            join dc in dbContext.DeployCI on dfp.DeployCIID equals dc.ID
                            select new
                            {
                                DeployCI = dc,
                                DeployProjectVersion = dpv
                            };

                var deployInfo = await query.SingleAsync();

                return await CIBuilder.Get(deployInfo.DeployCI.CIType).GetStatusByJobNumber(deployInfo.DeployCI.JsonConfig, deployInfo.DeployProjectVersion.CIJobIdentity);
            }
        }

        public async Task<DeployProjectVersionViewModel> GetDeployProjectVersionByDeployTaskID(int deployTaskID)
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from deployTask in dbContext.DeployTask.Where(dt => dt.ID == deployTaskID)
                            join deployProjectVersion in dbContext.DeployProjectVersion on deployTask.DeployProjectVersionID equals deployProjectVersion.ID
                            join user in dbContext.User on deployProjectVersion.CreateUserID equals user.ID
                            select new DeployProjectVersionViewModel
                            {
                                ID = deployProjectVersion.ID,
                                Number = deployProjectVersion.Number,
                                Description = deployProjectVersion.Description,
                                DeployProjectID = deployProjectVersion.DeployProjectID,
                                CIJobIdentity = deployProjectVersion.CIJobIdentity,
                                CreateUserID = deployProjectVersion.CreateUserID,
                                CreateTime = deployProjectVersion.CreateTime,
                                CreateUserName = user.Name
                            };

                return await query.SingleAsync();
            }
        }

        public async Task NotifyServerDownloadFilePackage(string deployServerIP, int ciID, string ciJobIdentity, string ciArtifactName)
        {
            var artifactInfo = await GetArtifactInfo(ciID, ciJobIdentity, ciArtifactName);

            using (var client = DeploySlaveClientWrapper.Get(deployServerIP))
            {
                var mapper = new MapperBase<ArtifactInfo, FilePackageInfo>();

                client.DownloadFilePackage(mapper.GetEntity(artifactInfo));
            }
        }

        public async Task<ArtifactInfo> GetArtifactInfo(int ciID, string ciJobIdentity, string ciArtifactName)
        {
            DeployCI deployCI = null;

            using (var dbContext = GDbContext.Create())
            {
                deployCI = dbContext.DeployCI.Single(dc => dc.ID == ciID);
            }

            var ici = CIBuilder.Get(deployCI.CIType);

            return (await ici.GetArtifactInfo(deployCI.JsonConfig, ciJobIdentity, ciArtifactName));
        }

        public float GetWindowsPerformanceMonitorValue(string categoryName, string counterName, string instanceName, string machineName)
        {
            try
            {
                var counter = new PerformanceCounter(categoryName, counterName, instanceName, machineName);

                return counter.NextValue();
            }
            catch (Exception ex)
            {
                //todo 暂时用实例名来忽略找不到实例的错误配置
                if (ex.Message.Contains(instanceName))
                {
                    return 1;
                }

                throw ex;
            }
        }

        public async Task<int> SetLoadBalanceServerWeightAsync(LoadBalanceType loadBalanceType, string loadBalanceValue, string serverIdentity, int weight)
        {
            if (loadBalanceType == LoadBalanceType.None)
                return 0;

            return await LoadBalanceBuilder.Get(loadBalanceType).SetServerWeight(loadBalanceValue, serverIdentity, weight);
        }

        public async Task ApplicationDeploy(string deployServerIP, DeployProjectType deployProjectType, DeployHostType deployProjectHost, DeployMode deployMode, string jsonHostProperty, int ciID, string ciJobIdentity, string ciArtifactName)
        {
            var filePackageName = (await GetArtifactInfo(ciID, ciJobIdentity, ciArtifactName)).Name;

            using (var client = DeploySlaveClientWrapper.Get(deployServerIP))
            {
                client.Deploy(new DeployInfo()
                {
                    FilePackageName = filePackageName,
                    DeployProjectType = deployProjectType,
                    DeployHostType = deployProjectHost,
                    DeployMode = deployMode,
                    JsonHostProperty = jsonHostProperty
                });
            }
        }

        public async Task<string> TriggerCache(string deployServerIP, string triggerCacheUrl)
        {
            using (var hc = new HttpClient())
            {
                hc.Timeout = new TimeSpan(0, 10, 0);
                var response = await hc.GetAsync(string.Format(triggerCacheUrl, deployServerIP));
                var content = await response.Content.ReadAsStringAsync();

                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new Exception(content);
                }

                return content;
            }
        }
    }
}
