﻿using Framework.Common.EnumOperation;
using Framework.Mapping.Base;
using G.Client.Data.Entities.DeployManage;
using G.Client.Data.Wrapper;
using G.Infrastructure.Constant.Enums;
using G.Infrastructure.Plugin;
using G.Client.Model.DeployManage.DeployEnvironmentModels;
using G.Client.Model.DeployManage.DeployManageModels;
using G.Client.Model.DeployManage.DeployProjectVersionModels;
using G.Client.Model.DeployManage.DeployTaskModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Business.DeployManage
{
    public class DeployManageBusiness
    {
        public async Task<DeployManageProjectViewModel> GetDeployProjectByID(int id)
        {
            var deployProject = await new DeployProjectBusiness().GetByID(id);

            return new DeployManageProjectViewModel()
            {
                DeployProject = deployProject
            };
        }

        public async Task<int> CreateDeployTask(CreateDeployTaskViewModel model)
        {
            using (var dbContext = GDbContext.Create())
            {
                string ciJobIdentity = model.CopyCIJobIdentity;
                if (string.IsNullOrWhiteSpace(ciJobIdentity))
                {
                    #region Call CI Build Job

                    var query = from deployFlowProperty in dbContext.DeployFlowProperty.Where(dfp => !dfp.IsDelete && dfp.DeployProjectID == model.DeployProjectID)
                                join dc in dbContext.DeployCI.Where(dc => !dc.IsDelete) on deployFlowProperty.DeployCIID equals dc.ID
                                select dc;

                    var deployCI = await query.SingleOrDefaultAsync();

                    if (deployCI != null)
                    {
                        var ici = CIBuilder.Get(deployCI.CIType);
                        ciJobIdentity = await ici?.BuildJob(deployCI.JsonConfig);
                    }

                    #endregion
                }

                using (var transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var deployProjectVersion = new DeployProjectVersion()
                        {
                            Number = model.ProjectVersionNumber,
                            Description = model.ProjectVersionDescription,
                            DeployProjectID = model.DeployProjectID,
                            CIJobIdentity = ciJobIdentity ?? "",
                        };

                        dbContext.DeployProjectVersion.Add(deployProjectVersion);

                        await dbContext.SaveChangesAsync();

                        var deployTask = new DeployTask()
                        {
                            DeployProjectVersionID = deployProjectVersion.ID,
                            Status = DeployTaskStatus.AsyncRun
                        };

                        dbContext.DeployTask.Add(deployTask);

                        await dbContext.SaveChangesAsync();

                        transaction.Commit();

                        return deployTask.ID;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();

                        throw ex;
                    }
                }
            }
        }

        public async Task<string> GetRecommendVersionNumber(int deployProjectID)
        {
            DeployProjectVersion deployProjectVersion = null;
            using (var dbContext = GDbContext.Create())
            {
                deployProjectVersion = await dbContext.DeployProjectVersion.Where(dpv => dpv.DeployProjectID == deployProjectID && !dpv.IsDelete).OrderByDescending(dpv => dpv.CreateTime).FirstOrDefaultAsync();
            }

            var lastNumber = deployProjectVersion?.Number ?? "1.0.0.-1";
            var splitedNumber = lastNumber.Split('.');

            splitedNumber[splitedNumber.Length - 1] = (int.Parse(splitedNumber[splitedNumber.Length - 1]) + 1).ToString();

            return string.Join(".", splitedNumber);
        }

        public async Task<List<DeployManageTaskViewModel>> GetDeployManageTaskList(int? deployProjectID = null)
        {
            using (var dbContext = GDbContext.Create())
            {
                Expression<Func<DeployProjectVersion, bool>> projectVersionExp = (dpv) => !dpv.IsDelete;
                projectVersionExp = deployProjectID.HasValue ? projectVersionExp.And(dpv => dpv.DeployProjectID == deployProjectID.Value) : projectVersionExp;

                var query = from deployTask in dbContext.DeployTask.Where(dt => !dt.IsDelete)
                            join deployProjectVersion in dbContext.DeployProjectVersion.Where(projectVersionExp) on deployTask.DeployProjectVersionID equals deployProjectVersion.ID
                            join deployProject in dbContext.DeployProject on deployProjectVersion.DeployProjectID equals deployProject.ID
                            join user in dbContext.User on deployProjectVersion.CreateUserID equals user.ID
                            select new DeployManageTaskViewModel()
                            {
                                DeployTask = new DeployTaskViewModel()
                                {
                                    ID = deployTask.ID,
                                    Status = deployTask.Status,
                                    DeployProjectVersionID = deployTask.DeployProjectVersionID,
                                    CreateUserID = deployTask.CreateUserID,
                                    CreateUserName = user.Name,
                                    CreateTime = deployTask.CreateTime,
                                },
                                DeployProjectVersion = new DeployProjectVersionViewModel()
                                {
                                    ID = deployProjectVersion.ID,
                                    Number = deployProjectVersion.Number,
                                    Description = deployProjectVersion.Description,
                                    DeployProjectID = deployProjectVersion.DeployProjectID,
                                    DeployProjectName = deployProject.Name,
                                    CIJobIdentity = deployProjectVersion.CIJobIdentity,
                                    CreateUserID = deployProjectVersion.CreateUserID,
                                }
                            };

                var lst = await query.ToListAsync();

                lst.ForEach(dmt =>
                {
                    dmt.DeployTask.StatusString = EnumHelper.GetEnumDescription(dmt.DeployTask.Status).Description;
                });

                return lst;
            }
        }

        public async Task<List<DeployEnvironmentViewModel>> GetEnvironmentListByDeployTaskID(int deployTaskID)
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from deployTask in dbContext.DeployTask.Where(dt => dt.ID == deployTaskID)
                            join deployProjectTask in dbContext.DeployProjectVersion on deployTask.DeployProjectVersionID equals deployProjectTask.ID
                            join deployEnvironment in dbContext.DeployEnvironment on deployProjectTask.DeployProjectID equals deployEnvironment.DeployProjectID
                            select deployEnvironment;

                var mapper = new MapperBase<DeployEnvironmentViewModel, DeployEnvironment>();

                var entityList = await query.ToListAsync();
                return mapper.GetModelList(entityList);
            }
        }

        public async Task<List<DeployProjectVersionViewModel>> GetProjectVersionListByDeployProjectID(int deployProjectID)
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from deployProjectVersion in dbContext.DeployProjectVersion.Where(dpv => dpv.DeployProjectID == deployProjectID)
                            join user in dbContext.User on deployProjectVersion.CreateUserID equals user.ID
                            select new DeployProjectVersionViewModel()
                            {
                                ID = deployProjectVersion.ID,
                                Number = deployProjectVersion.Number,
                                Description = deployProjectVersion.Description,
                                CIJobIdentity = deployProjectVersion.CIJobIdentity,
                                DeployProjectID = deployProjectVersion.DeployProjectID,
                                CreateUserID = deployProjectVersion.CreateUserID,
                                CreateUserName = user.Name,
                                CreateTime = deployProjectVersion.CreateTime,
                            };

                return await query.OrderByDescending(x => x.CreateTime).ToListAsync();
            }
        }

        public async Task<int> CreateDeployTaskByDeployProjectID(int deployProjectVersionID)
        {
            using (var dbContext = GDbContext.Create())
            {
                var deployTask = new DeployTask()
                {
                    DeployProjectVersionID = deployProjectVersionID,
                    Status = DeployTaskStatus.Duplicate
                };

                dbContext.DeployTask.Add(deployTask);

                await dbContext.SaveChangesAsync();

                return deployTask.ID;
            }
        }

        public async Task<List<CopyDeployProjectVersionViewModel>> GetCopyDeployProjectVersionList(int deployProjectID)
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from deployFlowPropertyTemp in dbContext.DeployFlowProperty.Where(dfp => !dfp.IsDelete && dfp.DeployProjectID == deployProjectID)
                            join deployFlowProperty in dbContext.DeployFlowProperty.Where(dfp => !dfp.IsDelete && dfp.DeployProjectID != deployProjectID) on deployFlowPropertyTemp.DeployCIID equals deployFlowProperty.DeployCIID
                            join deployProject in dbContext.DeployProject.Where(dp => !dp.IsDelete) on deployFlowProperty.DeployProjectID equals deployProject.ID
                            join deployProjectVersion in dbContext.DeployProjectVersion.Where(dpv => !dpv.IsDelete) on deployFlowProperty.ID equals deployProjectVersion.DeployProjectID
                            group new { deployProjectVersion, deployProject } by deployProjectVersion.CIJobIdentity into g
                            select new CopyDeployProjectVersionViewModel
                            {
                                CIJobIdentity = g.Key,
                                ProjectName = g.FirstOrDefault().deployProject.Name,
                                ProjectVersionID = g.FirstOrDefault().deployProjectVersion.ID,
                                ProjectVersionNumber = g.FirstOrDefault().deployProjectVersion.Number
                            };

                //只获取前20个项目版本
                return await query.OrderByDescending(cdpv => cdpv.ProjectVersionID).Take(20).ToListAsync();
            }
        }

        public async Task<string> GetConsoleUrlByJobNumber(int deployProjectVersionID)
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from dpv in dbContext.DeployProjectVersion.Where(dpv => dpv.ID == deployProjectVersionID)
                            join dfp in dbContext.DeployFlowProperty on dpv.DeployProjectID equals dfp.DeployProjectID
                            join dc in dbContext.DeployCI on dfp.DeployCIID equals dc.ID
                            select new
                            {
                                DeployCI = dc,
                                DeployProjectVersion = dpv
                            };

                var deployInfo = await query.SingleAsync();

                return await CIBuilder.Get(deployInfo.DeployCI.CIType).GetConsoleUrlByJobNumber(deployInfo.DeployCI.JsonConfig, deployInfo.DeployProjectVersion.CIJobIdentity);
            }
        }

        public async Task<int> SetLoadBalanceServerWeight(int deployProjectID, int deployServerID, int weight)
        {
            using (var dbContext = GDbContext.Create())
            {
                var serverIdentity = await (from deployServer in dbContext.DeployServer.Where(ds => ds.ID == deployServerID)
                                            select deployServer.ServerIdentity).SingleAsync();

                var loadBalanceInfo = await (from deployFlowProperty in dbContext.DeployFlowProperty.Where(dfp => dfp.DeployProjectID == deployProjectID)
                                             select new
                                             {
                                                 LoadBalanceType = deployFlowProperty.LoadBalanceType,
                                                 LoadBalanceValue = deployFlowProperty.LoadBalanceValue,
                                             }).SingleAsync();

                if (loadBalanceInfo.LoadBalanceType == LoadBalanceType.None)
                    return 0;

                var currentWeight = await new DeployRobotBusiness().SetLoadBalanceServerWeightAsync(
                    loadBalanceInfo.LoadBalanceType,
                    loadBalanceInfo.LoadBalanceValue,
                    serverIdentity,
                    weight);

                return currentWeight;
            }
        }

        public async Task<int> GetLoadBalanceServerWeight(int deployProjectID, int deployServerID)
        {
            using (var dbContext = GDbContext.Create())
            {
                var serverIdentity = await (from deployServer in dbContext.DeployServer.Where(ds => ds.ID == deployServerID)
                                            select deployServer.ServerIdentity).SingleAsync();

                var loadBalanceInfo = await (from deployFlowProperty in dbContext.DeployFlowProperty.Where(dfp => dfp.DeployProjectID == deployProjectID)
                                             select new
                                             {
                                                 LoadBalanceType = deployFlowProperty.LoadBalanceType,
                                                 LoadBalanceValue = deployFlowProperty.LoadBalanceValue,
                                             }).SingleAsync();

                if (loadBalanceInfo.LoadBalanceType == LoadBalanceType.None)
                    return 0;

                return await LoadBalanceBuilder.Get(loadBalanceInfo.LoadBalanceType).GetServerWeight(loadBalanceInfo.LoadBalanceValue, serverIdentity);
            }
        }
    }
}