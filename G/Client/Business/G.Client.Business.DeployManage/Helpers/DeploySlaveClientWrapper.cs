﻿using G.Client.Business.DeployManage.DeploySlaveService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Business.DeployManage.Helpers
{
    public class DeploySlaveClientWrapper
    {
        public static DeploySlaveClient Get(string serverIP)
        {
            //暂时硬编码
            return new DeploySlaveClient("BasicHttpBinding_IDeploySlave", $"http://{serverIP}:20001/DeploySlaveService/DeploySlave");
        }
    }
}