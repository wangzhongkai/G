﻿using G.Client.Data.Entities.DeployManage;
using G.Client.Data.Wrapper;
using G.Infrastructure.Data.Pipeline.Database;
using G.Client.Model.DeployManage.WindowsPerformanceMonitorModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Business.DeployManage
{
    public class WindowsPerformanceMonitorBusiness : SingleTableEngineer<WindowsPerformanceMonitor, int, WindowsPerformanceMonitorViewModel, CreateWindowsPerformanceMonitorViewModel, EditWindowsPerformanceMonitorViewModel>
    {
        public async Task<List<WindowsPerformanceMonitorViewModel>> GetManageViewList()
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from windowsPerformanceMonitor in dbContext.WindowsPerformanceMonitor.Where(wpm => !wpm.IsDelete)
                            join user in dbContext.User on windowsPerformanceMonitor.UpdateUserID equals user.ID
                            select new WindowsPerformanceMonitorViewModel()
                            {
                                ID = windowsPerformanceMonitor.ID,
                                Name = windowsPerformanceMonitor.Name,
                                CategoryName = windowsPerformanceMonitor.CategoryName,
                                CounterName = windowsPerformanceMonitor.CounterName,
                                InstanceName = windowsPerformanceMonitor.InstanceName,
                                MachineName = windowsPerformanceMonitor.MachineName,
                                ThresholdValue = windowsPerformanceMonitor.ThresholdValue,
                                UpdateUserID = windowsPerformanceMonitor.UpdateUserID,
                                UpdateUserName = user.Name,
                                UpdateTime = windowsPerformanceMonitor.UpdateTime
                            };

                return await query.OrderBy(wpm => wpm.Name).ToListAsync();
            }
        }
    }
}
