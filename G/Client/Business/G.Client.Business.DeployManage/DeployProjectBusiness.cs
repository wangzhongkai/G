﻿using G.Client.Data.Entities.DeployManage;
using G.Client.Data.Wrapper;
using G.Infrastructure.Data.Pipeline.Database;
using G.Client.Model.DeployManage.DeployProjectGroupModels;
using G.Client.Model.DeployManage.DeployProjectModels;
using G.Client.Model.DeployManage;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Framework.Mapping.Base;
using G.Client.Model.DeployManage.DeployProjectModels.DeployFlowPropertyModels;
using G.Client.Model.DeployManage.DeployCIModels;
using G.Client.Model.DeployManage.DeployRobotModels;

namespace G.Client.Business.DeployManage
{
    public class DeployProjectBusiness : SingleTableEngineer<DeployProject, int, DeployProjectManageViewModel, CreateDeployProjectViewModel, EditDeployProjectViewModel>
    {
        public async Task<List<DeployProjectManageViewModel>> GetManageViewList()
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from deployProject in dbContext.DeployProject.Where(dpg => !dpg.IsDelete)
                            join user in dbContext.User on deployProject.UpdateUserID equals user.ID
                            join deployProjectGroup in dbContext.DeployProjectGroup.Where(dp => !dp.IsDelete) on deployProject.DeployProjectGroupID equals deployProjectGroup.ID
                            select new DeployProjectManageViewModel()
                            {
                                ID = deployProject.ID,
                                Name = deployProject.Name,
                                DeployProjectGroupID = deployProjectGroup.ID,
                                DeployProjectGroupName = deployProjectGroup.Name,
                                Type = deployProject.Type,
                                UpdateTime = deployProject.UpdateTime,
                                UpdateUserID = deployProject.UpdateUserID,
                                UpdateUserName = user.Name,
                            };

                return await query.ToListAsync();
            }
        }

        public async override Task<int> Create(CreateDeployProjectViewModel model)
        {
            using (var dbContext = GDbContext.Create())
            {
                using (var transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var deployProjectMapper = new MapperBase<CreateDeployProjectViewModel, DeployProject>();
                        var deployProject = deployProjectMapper.GetEntity(model);

                        dbContext.DeployProject.Add(deployProject);
                        await dbContext.SaveChangesAsync();

                        var deployFlowPropertyMapper = new MapperBase<DeployFlowPropertyViewModel, DeployFlowProperty>();
                        var deployFlowProperty = deployFlowPropertyMapper.GetEntity(model.DeployFlowProperty);
                        deployFlowProperty.DeployProjectID = deployProject.ID;

                        dbContext.DeployFlowProperty.Add(deployFlowProperty);
                        await dbContext.SaveChangesAsync();

                        transaction.Commit();

                        return deployProject.ID;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        public async override Task<EditDeployProjectViewModel> Update(EditDeployProjectViewModel model)
        {
            using (var dbContext = GDbContext.Create())
            {
                var deployProject = await dbContext.DeployProject.SingleAsync(e => e.ID.Equals(model.ID));

                deployProject = new MapperBase<EditDeployProjectViewModel, DeployProject>().GetEntity(model, deployProject);
                deployProject.SetUpdateInfo();

                dbContext.DeployProject.Attach(deployProject);
                dbContext.Entry(deployProject).State = EntityState.Modified;

                var deployFlowProperty = await dbContext.DeployFlowProperty.SingleAsync(e => e.DeployProjectID.Equals(model.ID));

                model.DeployFlowProperty.ID = deployFlowProperty.ID;
                deployFlowProperty = new MapperBase<DeployFlowPropertyViewModel, DeployFlowProperty>().GetEntity(model.DeployFlowProperty, deployFlowProperty);
                deployFlowProperty.SetUpdateInfo();

                dbContext.DeployFlowProperty.Attach(deployFlowProperty);
                dbContext.Entry(deployFlowProperty).State = EntityState.Modified;

                await dbContext.SaveChangesAsync();

                return model;
            }
        }

        public async override Task<EditDeployProjectViewModel> GetByID(int id)
        {
            var EditDeployProjectViewModel = await base.GetByID(id);

            using (var dbContext = GDbContext.Create())
            {
                var queryDeployFlowProperty = from deployFlowProperty in dbContext.DeployFlowProperty.Where(dfp => dfp.DeployProjectID == EditDeployProjectViewModel.ID)
                                              select deployFlowProperty;

                var deployFlowPropertyMapper = new MapperBase<DeployFlowPropertyViewModel, DeployFlowProperty>();
                EditDeployProjectViewModel.DeployFlowProperty = deployFlowPropertyMapper.GetModel(await queryDeployFlowProperty.SingleAsync());
            }

            return EditDeployProjectViewModel;
        }

        public async Task<DeployRobotDeployProjectViewModel> GetDeployProjectView(int deployProjectID)
        {
            var view = new DeployRobotDeployProjectViewModel();
            view.DeployProject = await GetByID(deployProjectID);

            if (view.DeployProject.DeployFlowProperty.WindowsPerformanceMonitorID > 0)
            {
                view.WindowsPerformanceMonitor = await new WindowsPerformanceMonitorBusiness().GetByID(view.DeployProject.DeployFlowProperty.WindowsPerformanceMonitorID);
            }

            if (view.DeployProject.DeployFlowProperty.DeployCIID > 0)
            {
                view.DeployCI = await new DeployCIBusiness().GetByID(view.DeployProject.DeployFlowProperty.DeployCIID);
            }

            return view;
        }
    }
}
