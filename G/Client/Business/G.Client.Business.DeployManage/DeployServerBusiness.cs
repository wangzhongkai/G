﻿using Framework.Mapping.Base;
using G.Client.Data.Entities.DeployManage;
using G.Client.Data.Wrapper;
using G.Client.Model.DeployManage.DeployServerModels;
using G.Infrastructure.Constant.Enums;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace G.Client.Business.DeployManage
{
    public class DeployServerBusiness
    {
        public async Task<List<DeployServerViewModel>> GetList()
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from deployServer in dbContext.DeployServer.Where(ds => !ds.IsDelete)
                            join user in dbContext.User on deployServer.UpdateUserID equals user.ID
                            select new DeployServerViewModel()
                            {
                                ID = deployServer.ID,
                                Name = deployServer.Name,
                                IPAddress = deployServer.IPAddress,
                                ServerIdentity = deployServer.ServerIdentity,
                                Status = deployServer.Status,
                                UpdateUserID = deployServer.UpdateUserID,
                                UpdateUserName = user.Name,
                                UpdateTime = deployServer.UpdateTime,
                            };

                return await query.ToListAsync();
            }
        }

        public async Task<EditDeployServerViewModel> GetByID(int id)
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = (from deployServer in dbContext.DeployServer.Where(ds => ds.ID == id)
                             join deployEnvironmentServer in dbContext.DeployEnvironmentServer.Where(des => !des.IsDelete) on deployServer.ID equals deployEnvironmentServer.DeployServerID into deployEnvironmentServers
                             select new EditDeployServerViewModel()
                             {
                                 ID = deployServer.ID,
                                 IPAddress = deployServer.IPAddress,
                                 Name = deployServer.Name,
                                 ServerIdentity = deployServer.ServerIdentity,
                                 Status = deployServer.Status,
                                 DeployEnvironmentIDList = deployEnvironmentServers.Select(des => des.DeployEnvironmentID).ToList()
                             });

                return await query.SingleOrDefaultAsync();
            }
        }

        public async Task<int> Create(CreateDeployServerViewModel model)
        {
            using (var dbContext = GDbContext.Create())
            {
                var deployServer = new DeployServer()
                {
                    Name = model.Name,
                    ServerIdentity = model.ServerIdentity,
                    IPAddress = model.IPAddress,
                    Status = model.Status
                };
                dbContext.DeployServer.Add(deployServer);

                using (var transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        await dbContext.SaveChangesAsync();

                        foreach (var deployEnvironmentID in model.DeployEnvironmentIDList)
                        {
                            dbContext.DeployEnvironmentServer.Add(new DeployEnvironmentServer()
                            {
                                DeployServerID = deployServer.ID,
                                DeployEnvironmentID = deployEnvironmentID
                            });
                        }

                        await dbContext.SaveChangesAsync();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }

                return deployServer.ID;
            }
        }

        public async Task<EditDeployServerViewModel> Update(EditDeployServerViewModel model)
        {
            using (var dbContext = GDbContext.Create())
            {
                var entity = await dbContext.DeployServer.SingleAsync(ds => ds.ID == model.ID);

                entity = new MapperBase<EditDeployServerViewModel, DeployServer>().GetEntity(model, entity);
                entity.SetUpdateInfo();

                dbContext.DeployServer.Attach(entity);
                dbContext.Entry(entity).State = EntityState.Modified;

                using (var transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        await dbContext.SaveChangesAsync();

                        #region 更新环境服务器

                        var oldDeployEnvironmentList = await dbContext.DeployEnvironmentServer.Where(des => !des.IsDelete && des.DeployServerID == model.ID).ToListAsync();

                        var intersectDeployEnvironmentIDList = oldDeployEnvironmentList.Select(de => de.ID).Intersect(model.DeployEnvironmentIDList).ToList();
                        var needDeleteDeployEnvironmentIDList = oldDeployEnvironmentList.Select(de => de.ID).Except(intersectDeployEnvironmentIDList).ToList();
                        var needAddDeployEnvironmentIDList = model.DeployEnvironmentIDList.Except(intersectDeployEnvironmentIDList).ToList();

                        needDeleteDeployEnvironmentIDList.ForEach(deployEnvironmentID =>
                        {
                            var deployEnvironmentServer = oldDeployEnvironmentList.Single(de => de.ID == deployEnvironmentID);
                            deployEnvironmentServer.IsDelete = true;
                            deployEnvironmentServer.SetUpdateInfo();
                            dbContext.DeployEnvironmentServer.Attach(deployEnvironmentServer);
                            dbContext.Entry(deployEnvironmentServer).State = EntityState.Modified;
                        });

                        needAddDeployEnvironmentIDList.ForEach(deployEnvironmentID =>
                        {
                            var deployEnvironmentServer = new DeployEnvironmentServer()
                            {
                                DeployServerID = model.ID,
                                DeployEnvironmentID = deployEnvironmentID
                            };

                            dbContext.DeployEnvironmentServer.Add(deployEnvironmentServer);
                        });

                        await dbContext.SaveChangesAsync();

                        #endregion

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }

                return model;
            }
        }

        public async Task<List<EditDeployServerViewModel>> GetDeployServerListByDeployEnvironmentID(int deployEnvironmentID)
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from deployEnvironmentServer in dbContext.DeployEnvironmentServer.Where(des => !des.IsDelete && des.DeployEnvironmentID == deployEnvironmentID)
                            join deployServer in dbContext.DeployServer.Where(ds => !ds.IsDelete && ds.Status == DeployServerStatus.Online) on deployEnvironmentServer.DeployServerID equals deployServer.ID
                            select deployServer;

                var mapper = new MapperBase<EditDeployServerViewModel, DeployServer>();

                return mapper.GetModelList(await query.OrderBy(ds => ds.Name).ToListAsync());
            }
        }
    }
}