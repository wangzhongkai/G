﻿using G.Client.Data.Entities.DeployManage;
using G.Client.Data.Wrapper;
using G.Infrastructure.Data.Pipeline.Database;
using G.Client.Model.DeployManage.DeployProjectGroupModels;
using G.Client.Model.DeployManage.DeployProjectModels;
using G.Client.Model.DeployManage;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Business.DeployManage
{
    public class DeployProjectGroupBusiness : SingleTableEngineer<DeployProjectGroup, int, DeployProjectGroupViewModel, CreateDeployProjectGroupViewModel, EditDeployProjectGroupViewModel>
    {
        public async Task<List<DeployProjectGroupManageViewModel>> GetManageViewList()
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from deployProjectGroup in dbContext.DeployProjectGroup.Where(dpg => !dpg.IsDelete)
                            join deployProject in dbContext.DeployProject.Where(dp => !dp.IsDelete) on deployProjectGroup.ID equals deployProject.DeployProjectGroupID into deployProjects
                            from deployProject in deployProjects.DefaultIfEmpty()
                            group new { ID = deployProjectGroup.ID, Name = deployProjectGroup.Name, DeployProject = deployProject } by new { ID = deployProjectGroup.ID, Name = deployProjectGroup.Name } into dpg
                            select new DeployProjectGroupManageViewModel()
                            {
                                ID = dpg.Key.ID,
                                Name = dpg.Key.Name,
                                DeployProjectList = dpg.Where(d => d != null && d.DeployProject != null).Select(d => new DeployProjectManageViewModel()
                                {
                                    ID = d.DeployProject.ID,
                                    Name = d.DeployProject.Name
                                }).ToList()
                            };

                return await query.ToListAsync();
            }
        }

        public async Task<DeployProjectGroupManageViewModel> GetManageViewByID(int id)
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from deployProjectGroup in dbContext.DeployProjectGroup.Where(dpg => dpg.ID == id)
                            join deployProject in dbContext.DeployProject.Where(dp => !dp.IsDelete) on deployProjectGroup.ID equals deployProject.DeployProjectGroupID into deployProjects
                            from deployProject in deployProjects.DefaultIfEmpty()
                            group new { ID = deployProjectGroup.ID, Name = deployProjectGroup.Name, DeployProject = deployProject } by new { ID = deployProjectGroup.ID, Name = deployProjectGroup.Name } into dpg
                            select new DeployProjectGroupManageViewModel()
                            {
                                ID = dpg.Key.ID,
                                Name = dpg.Key.Name,
                                DeployProjectList = dpg.Where(d => d != null && d.DeployProject != null).Select(d => new DeployProjectManageViewModel()
                                {
                                    ID = d.DeployProject.ID,
                                    Name = d.DeployProject.Name
                                }).ToList()
                            };

                return await query.SingleAsync();
            }
        }
    }
}
