﻿using G.Client.Data.Entities.DeployManage;
using G.Client.Data.Wrapper;
using G.Infrastructure.Data.Pipeline.Database;
using G.Client.Model.DeployManage.DeployCIModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Business.DeployManage
{
    public class DeployCIBusiness : SingleTableEngineer<DeployCI, int, DeployCIViewModel, CreateDeployCIViewModel, EditDeployCIViewModel>
    {
        public async override Task<List<DeployCIViewModel>> GetList()
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from deployCI in dbContext.DeployCI.Where(dc => !dc.IsDelete)
                            join user in dbContext.User on deployCI.UpdateUserID equals user.ID
                            select new DeployCIViewModel()
                            {
                                ID = deployCI.ID,
                                Name = deployCI.Name,
                                CIType = deployCI.CIType,
                                JsonConfig = deployCI.JsonConfig,
                                UpdateUserID = deployCI.UpdateUserID,
                                UpdateUserName = user.Name,
                                UpdateTime = deployCI.UpdateTime
                            };

                return await query.ToListAsync();
            }
        }
    }
}