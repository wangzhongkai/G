﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using G.Client.Data.Wrapper;
using G.Client.Data.Entities.Accounts;
using System.Data.Entity;

namespace G.Client.Model.Account.IdentityModels
{
    public class UserStore : IUserStore<ApplicationUser>, IUserPasswordStore<ApplicationUser>
    {
        public async Task CreateAsync(ApplicationUser user)
        {
            using (var dbContext = GDbContext.Create())
            {
                dbContext.User.Add(new User()
                {
                    UserName = user.UserName,
                    Password = user.Password,
                    Name = user.Name,
                    Role = user.Role,
                });

                await dbContext.SaveChangesAsync();
            }
        }

        public Task DeleteAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            //nothing to do
        }

        public Task<ApplicationUser> FindByIdAsync(string userId)
        {
            int id = 0;
            if (!Int32.TryParse(userId, out id))
            {
                throw new ArgumentException(nameof(userId));
            }

            using (var dbContext = GDbContext.Create())
            {
                var user = dbContext.User.SingleOrDefault(u => u.ID == id && !u.IsDelete);

                return Task.FromResult(new ApplicationUser()
                {
                    Id = user.ID.ToString(),
                    Name = user.Name,
                    Password = user.Password,
                    Role = user.Role,
                    UserName = user.UserName
                });
            }
        }

        public Task<ApplicationUser> FindByNameAsync(string userName)
        {
            using (var dbContext = GDbContext.Create())
            {
                var user = dbContext.User.SingleOrDefault(u => u.UserName == userName && !u.IsDelete);

                if (user == null)
                    return Task.FromResult<ApplicationUser>(null);

                return Task.FromResult(new ApplicationUser()
                {
                    Id = user.ID.ToString(),
                    Name = user.Name,
                    Password = user.Password,
                    Role = user.Role,
                    UserName = user.UserName
                });
            }
        }

        public async Task<string> GetPasswordHashAsync(ApplicationUser user)
        {
            using (var dbContext = GDbContext.Create())
            {
                var userID = int.Parse(user.Id);
                var entity = await dbContext.User.SingleOrDefaultAsync(u => u.ID == userID && !u.IsDelete);

                return entity.Password;
            }
        }

        public Task<bool> HasPasswordAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetPasswordHashAsync(ApplicationUser user, string passwordHash)
        {
            user.Password = passwordHash;

            return Task.FromResult(0);
        }

        public async Task UpdateAsync(ApplicationUser user)
        {
            using (var dbContext = GDbContext.Create())
            {
                var userID = int.Parse(user.Id);
                var entity = dbContext.User.SingleOrDefault(u => u.ID == userID && !u.IsDelete);

                entity.UserName = user.UserName;
                entity.Password = user.Password;
                entity.Role = user.Role;
                entity.SetUpdateInfo();

                dbContext.User.Attach(entity);
                dbContext.Entry(entity).State = EntityState.Modified;

                await dbContext.SaveChangesAsync();
            }
        }
    }
}