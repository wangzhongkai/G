﻿using G.Infrastructure.Constant.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployProjectModels
{
    public class EditDeployProjectViewModel : CreateDeployProjectViewModel, IEditViewModel<int>
    {
        public int ID { get; set; }
    }
}
