﻿using G.Infrastructure.Constant.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployProjectModels
{
    public class DeployProjectManageViewModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public int DeployProjectGroupID { get; set; }

        public string DeployProjectGroupName { get; set; }

        public DeployProjectType Type { get; set; }

        public int UpdateUserID { get; set; }

        public string UpdateUserName { get; set; }

        public DateTime UpdateTime { get; set; }
    }
}
