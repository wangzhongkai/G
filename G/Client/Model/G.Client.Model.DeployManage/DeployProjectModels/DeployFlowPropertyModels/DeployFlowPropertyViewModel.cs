﻿using G.Infrastructure.Constant.Enums;
using G.Client.Model.DeployManage.DeployCIModels;
using G.Client.Model.DeployManage.WindowsPerformanceMonitorModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployProjectModels.DeployFlowPropertyModels
{
    public class DeployFlowPropertyViewModel
    {
        public int ID { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "持续集成不能为空")]
        public int DeployCIID { get; set; }

        [StringLength(100)]
        public string DeployCIArtifactName { get; set; }

        public LoadBalanceType LoadBalanceType { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false, NullDisplayText = "")]
        [RequiredIf("LoadBalanceType", Value = LoadBalanceType.Aliyun, ErrorMessage = "负载均衡为阿里云时，负载均衡值不能为空")]
        public string LoadBalanceValue { get; set; }

        public int WindowsPerformanceMonitorID { get; set; }

        public int WindowsPerformanceMonitorCheckMaximumLatency { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false, NullDisplayText = "")]
        public string TriggerCacheUrl { get; set; }

        public DeployHostType Host { get; set; }

        public DeployMode DeployMode { get; set; }

        [Required(AllowEmptyStrings = true)]
        [StringLength(int.MaxValue, MinimumLength = 1)]
        public string JsonHostProperty { get; set; }
    }
}