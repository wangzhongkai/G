﻿using G.Infrastructure.Constant.Enums;
using G.Client.Model.DeployManage.DeployProjectModels.DeployFlowPropertyModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployProjectModels
{
    public class CreateDeployProjectViewModel
    {
        [Required, StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        public int DeployProjectGroupID { get; set; }

        public DeployProjectType Type { get; set; }

        public int AutoRetryDeployTime { get; set; }

        [Range(1, int.MaxValue)]
        public int DeployExceptionThreshold { get; set; }

        private int[] _deployFlowTypeArray = null;

        public int[] DeployFlowTypeArray
        {
            get
            {
                return _deployFlowTypeArray;
            }
            set
            {
                _deployFlowTypeArray = value;

                int deployFlowType = 0;
                _deployFlowTypeArray?.ToList().ForEach(dft =>
                {
                    deployFlowType |= dft;
                });

                if (deployFlowType > 0)
                {
                    DeployFlow = deployFlowType;
                }
            }
        }

        public int DeployFlow { get; set; }

        public DeployFlowPropertyViewModel DeployFlowProperty { get; set; }
    }
}