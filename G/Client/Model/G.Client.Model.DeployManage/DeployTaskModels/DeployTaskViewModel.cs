﻿using G.Infrastructure.Constant.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployTaskModels
{
    public class DeployTaskViewModel
    {
        public int ID { get; set; }

        public int DeployProjectVersionID { get; set; }

        public DeployTaskStatus Status { get; set; }

        public string StatusString { get; set; }

        public int CreateUserID { get; set; }

        public string CreateUserName { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
