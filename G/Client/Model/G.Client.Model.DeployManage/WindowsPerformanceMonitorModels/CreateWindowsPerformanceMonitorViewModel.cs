﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.WindowsPerformanceMonitorModels
{
    public class CreateWindowsPerformanceMonitorViewModel
    {
        [Required, StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        [Required, StringLength(100, MinimumLength = 1)]
        public string CategoryName { get; set; }

        [Required, StringLength(100, MinimumLength = 1)]
        public string CounterName { get; set; }

        [Required, StringLength(100, MinimumLength = 1)]
        public string InstanceName { get; set; }

        [Required(AllowEmptyStrings = true)]
        [DisplayFormat(ConvertEmptyStringToNull = false, NullDisplayText = "")]
        public string MachineName { get; set; }

        public double ThresholdValue { get; set; }
    }
}
