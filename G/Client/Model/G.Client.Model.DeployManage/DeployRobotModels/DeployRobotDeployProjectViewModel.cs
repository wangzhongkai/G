﻿using G.Client.Model.DeployManage.DeployCIModels;
using G.Client.Model.DeployManage.DeployProjectModels;
using G.Client.Model.DeployManage.WindowsPerformanceMonitorModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployRobotModels
{
    public class DeployRobotDeployProjectViewModel
    {
        public EditDeployProjectViewModel DeployProject { get; set; }

        public EditWindowsPerformanceMonitorViewModel WindowsPerformanceMonitor { get; set; }

        public EditDeployCIViewModel DeployCI { get; set; }
    }
}
