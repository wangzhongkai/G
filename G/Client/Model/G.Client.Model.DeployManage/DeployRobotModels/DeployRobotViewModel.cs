﻿using G.Client.Model.DeployManage.DeployEnvironmentModels;
using G.Client.Model.DeployManage.DeployProjectVersionModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployRobotModels
{
    public class DeployRobotViewModel
    {
        public DeployEnvironmentManageItemViewModel DeployEnvironment { get; set; }

        public DeployProjectVersionViewModel DeployProjectVersion { get; set; }
    }
}
