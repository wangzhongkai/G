﻿using G.Client.Model.DeployManage.DeployProjectModels;
using G.Client.Model.DeployManage.DeployServerModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployProjectGroupModels
{
    public class DeployProjectGroupManageViewModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public List<DeployProjectManageViewModel> DeployProjectList { get; set; }
    }
}
