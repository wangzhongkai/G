﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployProjectGroupModels
{
    public class EditDeployProjectGroupViewModel: CreateDeployProjectGroupViewModel, IEditViewModel<int>
    {
        public int ID { get; set; }
    }
}
