﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployEnvironmentTagModels
{
    public class CreateDeployEnvironmentTagViewModel
    {
        public string Name { get; set; }
    }
}
