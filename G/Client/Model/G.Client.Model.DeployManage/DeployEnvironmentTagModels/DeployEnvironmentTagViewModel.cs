﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployEnvironmentTagModels
{
    public class DeployEnvironmentTagViewModel : CreateDeployEnvironmentTagViewModel
    {
        public int ID { get; set; }

        public int UpdateUserID { get; set; }

        public string UpdateUserName { get; set; }

        public DateTime UpdateTime { get; set; }
    }
}
