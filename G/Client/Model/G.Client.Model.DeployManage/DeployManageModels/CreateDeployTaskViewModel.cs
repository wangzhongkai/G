﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployManageModels
{
    public class CreateDeployTaskViewModel
    {
        public string ProjectVersionNumber { get; set; }

        public string ProjectVersionDescription { get; set; }

        public int DeployProjectID { get; set; }

        public int DeployEnvironmentID { get; set; }

        public string CopyCIJobIdentity { get; set; }
    }
}
