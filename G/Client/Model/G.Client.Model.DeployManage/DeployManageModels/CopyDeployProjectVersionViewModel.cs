﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployManageModels
{
    public class CopyDeployProjectVersionViewModel
    {
        public string CIJobIdentity { get; set; }

        public string ProjectName { get; set; }

        public int ProjectVersionID { get; set; }

        public string ProjectVersionNumber { get; set; }
    }
}
