﻿using G.Infrastructure.Constant.Enums;
using G.Client.Model.DeployManage.DeployProjectVersionModels;
using G.Client.Model.DeployManage.DeployTaskModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployManageModels
{
    public class DeployManageTaskViewModel
    {
        public DeployTaskViewModel DeployTask { get; set; }

        public DeployProjectVersionViewModel DeployProjectVersion { get; set; }
    }
}
