﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployCIModels
{
    public class EditDeployCIViewModel : CreateDeployCIViewModel, IEditViewModel<int>
    {
        public int ID { get; set; }
    }
}