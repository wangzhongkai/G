﻿using G.Infrastructure.Constant.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployCIModels
{
    public class CreateDeployCIViewModel
    {
        [Required, StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        public CIType CIType { get; set; }

        [Required(AllowEmptyStrings = true)]
        [DisplayFormat(ConvertEmptyStringToNull = false, NullDisplayText = "")]
        public string JsonConfig { get; set; }
    }
}
