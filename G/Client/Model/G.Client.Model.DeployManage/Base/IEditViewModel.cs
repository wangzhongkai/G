﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage
{
    public interface IEditViewModel<TKey>
    {
        TKey ID { get; set; }
    }
}
