﻿using G.Client.Model.DeployManage.DeployServerModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployEnvironmentModels
{
    public class DeployEnvironmentManageViewModel
    {
        public int DeployEnvironmentTagID { get; set; }

        public string DeployEnvironmentTagName { get; set; }

        public List<DeployEnvironmentManageItemViewModel> DeployEnvironmentManageItemViewModelList { get; set; }
    }

    public class DeployEnvironmentManageItemViewModel : CreateDeployEnvironmentViewModel
    {
        public int ID { get; set; }

        public string DeployProjectName { get; set; }

        public List<DeployServerViewModel> DeployServerList { get; set; }
    }
}
