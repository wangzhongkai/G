﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployEnvironmentModels
{
    public class CreateDeployEnvironmentViewModel
    {
        [Required, StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        [Range(1, int.MaxValue)]
        public int DeployEnvironmentTagID { get; set; }

        [Range(1, int.MaxValue)]
        public int DeployProjectID { get; set; }
    }
}
