﻿using G.Client.Model.DeployManage.DeployServerModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployEnvironmentModels
{
    public class DeployEnvironmentViewModel : CreateDeployEnvironmentViewModel
    {
        public int ID { get; set; }
    }
}
