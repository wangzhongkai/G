﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Model.DeployManage.DeployProjectVersionModels
{
    public class DeployProjectVersionViewModel
    {
        public int ID { get; set; }

        public string Number { get; set; }

        public string Description { get; set; }

        public int DeployProjectID { get; set; }

        public string DeployProjectName { get; set; }

        public string CIJobIdentity { get; set; }

        public int CreateUserID { get; set; }

        public string CreateUserName { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
