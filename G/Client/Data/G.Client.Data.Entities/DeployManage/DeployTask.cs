﻿using G.Client.Data.Entities.Base;
using G.Infrastructure.Constant.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Data.Entities.DeployManage
{
    public class DeployTask : EntityBase<int>
    {
        public int DeployProjectVersionID { get; set; }

        public DeployTaskStatus Status { get; set; }
    }
}
