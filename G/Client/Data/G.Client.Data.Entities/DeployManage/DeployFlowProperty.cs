﻿using G.Client.Data.Entities.Base;
using G.Infrastructure.Constant.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Data.Entities.DeployManage
{
    public class DeployFlowProperty : EntityBase<int>
    {
        public int DeployProjectID { get; set; }

        public int DeployCIID { get; set; }

        [StringLength(100)]
        public string DeployCIArtifactName { get; set; }

        public LoadBalanceType LoadBalanceType { get; set; }

        public string LoadBalanceValue { get; set; }

        public int WindowsPerformanceMonitorID { get; set; }

        public int WindowsPerformanceMonitorCheckMaximumLatency { get; set; }

        public string TriggerCacheUrl { get; set; }

        public DeployHostType Host { get; set; }

        public DeployMode DeployMode { get; set; }

        [Required(AllowEmptyStrings = true)]
        [StringLength(int.MaxValue, MinimumLength = 1)]
        public string JsonHostProperty { get; set; }
    }
}