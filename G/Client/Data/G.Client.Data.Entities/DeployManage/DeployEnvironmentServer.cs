﻿using G.Client.Data.Entities.Base;
using G.Infrastructure.Constant.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace G.Client.Data.Entities.DeployManage
{
    public class DeployEnvironmentServer : EntityBase<int>
    {
        public int DeployEnvironmentID { get; set; }

        public int DeployServerID { get; set; }
    }
}