﻿using G.Client.Data.Entities.Base;
using G.Infrastructure.Constant.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Client.Data.Entities.DeployManage
{
    public class DeployProjectVersion : EntityBase<int>
    {
        [Required, StringLength(50, MinimumLength = 1)]
        public string Number { get; set; }

        [Required(AllowEmptyStrings = true)]
        public string Description { get; set; }

        public int DeployProjectID { get; set; }

        [Required(AllowEmptyStrings = true), StringLength(200)]
        public string CIJobIdentity { get; set; }
    }
}
