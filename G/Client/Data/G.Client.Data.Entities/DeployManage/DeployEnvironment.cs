﻿using G.Client.Data.Entities.Base;
using G.Infrastructure.Constant.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace G.Client.Data.Entities.DeployManage
{
    public class DeployEnvironment : EntityBase<int>
    {
        [Required, StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        public int DeployEnvironmentTagID { get; set; }

        public int DeployProjectID { get; set; }
    }
}