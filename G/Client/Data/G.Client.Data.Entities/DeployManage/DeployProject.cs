﻿using G.Client.Data.Entities.Base;
using G.Infrastructure.Constant.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace G.Client.Data.Entities.DeployManage
{
    public class DeployProject : EntityBase<int>
    {
        [Required, StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        public int DeployProjectGroupID { get; set; }

        public DeployProjectType Type { get; set; }

        public int AutoRetryDeployTime { get; set; }

        public int DeployExceptionThreshold { get; set; }

        public int DeployFlow { get; set; }
    }
}