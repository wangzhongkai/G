﻿using G.Client.Data.Entities.Base;
using G.Infrastructure.Constant.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace G.Client.Data.Entities.DeployManage
{
    public class WindowsPerformanceMonitor : EntityBase<int>
    {
        [Required, StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        [Required, StringLength(100, MinimumLength = 1)]
        public string CategoryName { get; set; }

        [Required, StringLength(100, MinimumLength = 1)]
        public string CounterName { get; set; }

        [Required, StringLength(100, MinimumLength = 1)]
        public string InstanceName { get; set; }

        [Required(AllowEmptyStrings = true)]
        public string MachineName { get; set; }

        public double ThresholdValue { get; set; }
    }
}