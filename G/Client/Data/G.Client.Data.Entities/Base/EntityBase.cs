﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace G.Client.Data.Entities.Base
{
    public class EntityBase<TPrimaryKey>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public TPrimaryKey ID { get; set; }

        [Required]
        public bool IsDelete { get; set; } = false;

        public int CreateUserID { get; set; }

        public int UpdateUserID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; } = DateTime.Now;

        public EntityBase()
        {
            var userid = HttpContext.Current?.User?.Identity?.GetUserId();
            if (userid != null)
                CreateUserID = Int32.Parse(userid);

            UpdateUserID = CreateUserID;
        }

        public void SetUpdateInfo()
        {
            var userid = HttpContext.Current?.User?.Identity?.GetUserId();
            if (userid != null)
                UpdateUserID = Int32.Parse(userid);

            UpdateTime = DateTime.Now;
        }
    }
}