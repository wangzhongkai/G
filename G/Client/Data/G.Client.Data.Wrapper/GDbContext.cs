﻿using G.Client.Data.Entities.Accounts;
using G.Client.Data.Entities.Base;
using G.Client.Data.Entities.DeployManage;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;

namespace G.Client.Data.Wrapper
{
    public class GDbContext : DbContext
    {
        static GDbContext()
        {
            //修复EF6从Nuget中添加的Bug
            var _ = typeof(System.Data.Entity.SqlServer.SqlProviderServices);

            string dbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "DB");

            //重定向数据目录，方便测试
            AppDomain.CurrentDomain.SetData("DataDirectory", dbPath);
        }

        private GDbContext()
            : base("name=DefaultConnection")
        {
            Configuration.AutoDetectChangesEnabled = false;
            //关闭EF6.x 默认自动生成null判断语句
            Configuration.UseDatabaseNullSemantics = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //todo 自动注册
            modelBuilder.Entity<User>().ToTable("dbo.User");
            modelBuilder.Entity<DeployServer>().ToTable("dbo.DeployServer");
            modelBuilder.Entity<DeployEnvironment>().ToTable("dbo.DeployEnvironment");
            modelBuilder.Entity<DeployEnvironmentServer>().ToTable("dbo.DeployEnvironmentServer");
            modelBuilder.Entity<DeployEnvironmentTag>().ToTable("dbo.DeployEnvironmentTag");
            modelBuilder.Entity<DeployProjectGroup>().ToTable("dbo.DeployProjectGroup");
            modelBuilder.Entity<DeployProject>().ToTable("dbo.DeployProject");
            modelBuilder.Entity<DeployFlowProperty>().ToTable("dbo.DeployFlowProperty");
            modelBuilder.Entity<DeployProjectVersion>().ToTable("dbo.DeployProjectVersion");
            modelBuilder.Entity<DeployTask>().ToTable("dbo.DeployTask");
            modelBuilder.Entity<DeployCI>().ToTable("dbo.DeployCI");
            modelBuilder.Entity<WindowsPerformanceMonitor>().ToTable("dbo.WindowsPerformanceMonitor");
        }

        public static GDbContext Create()
        {
            return new GDbContext();
        }

        public static DbSet<TEntity> GetDbSet<TEntity, TKey>(GDbContext context)
            where TEntity : EntityBase<TKey>
        {
            return context.Set<TEntity>();
        }

        public DbSet<User> User { get; set; }

        public DbSet<DeployServer> DeployServer { get; set; }

        public DbSet<DeployEnvironment> DeployEnvironment { get; set; }

        public DbSet<DeployEnvironmentServer> DeployEnvironmentServer { get; set; }

        public DbSet<DeployEnvironmentTag> DeployEnvironmentTag { get; set; }

        public DbSet<DeployProjectGroup> DeployProjectGroup { get; set; }

        public DbSet<DeployProject> DeployProject { get; set; }

        public DbSet<DeployFlowProperty> DeployFlowProperty { get; set; }

        public DbSet<DeployProjectVersion> DeployProjectVersion { get; set; }

        public DbSet<DeployTask> DeployTask { get; set; }

        public DbSet<DeployCI> DeployCI { get; set; }

        public DbSet<WindowsPerformanceMonitor> WindowsPerformanceMonitor { get; set; }
    }
}