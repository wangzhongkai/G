﻿using Framework.Wcf.Attributes;
using Framework.Wcf.ExceptionExtension;
using G.Service.Contract.DeploySlave.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using G.Service.Business.DeploySlave;
using G.Service.Model.DeploySlave.ProcessManage;

namespace G.Service.Contract.DeploySlave.Services
{
    [WcfService]
    [GlobalExceptionHandlerBehaviour(typeof(GlobalExceptionHandler))]
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ProcessManageService : IProcessManage
    {
        public int Run(RunProcessInfo runProcessInfo)
        {
            return new ProcessManageBusiness().Run(runProcessInfo);
        }

        public bool IsRunning(CheckProcessStatusInfo checkProcessStatusInfo)
        {
            return new ProcessManageBusiness().IsRunning(checkProcessStatusInfo);
        }

        public bool Kill(KillProcessInfo killProcessInfo)
        {
            return new ProcessManageBusiness().Kill(killProcessInfo);
        }
    }
}
