﻿using Framework.Wcf.Attributes;
using Framework.Wcf.ExceptionExtension;
using G.Infrastructure.Contract.Deploy.Model;
using G.Service.Business.DeploySlave;
using G.Service.Contract.DeploySlave.Interfaces;
using G.Service.Model.DeploySlave.Deploy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace G.Service.Contract.DeploySlave.Services
{
    [WcfService]
    [GlobalExceptionHandlerBehaviour(typeof(GlobalExceptionHandler))]
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class DeploySlaveService : IDeploySlave
    {
        public void DownloadFilePackage(FilePackageInfo filePackageInfo)
        {
            new DeploySlaveBusiness().DownloadFilePackage(filePackageInfo);
        }

        public void Deploy(DeployInfo deployInfo)
        {
            new DeploySlaveBusiness().Deploy(deployInfo);
        }
    }
}
