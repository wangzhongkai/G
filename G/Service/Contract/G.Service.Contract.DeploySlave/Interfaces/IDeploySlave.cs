﻿using Framework.Wcf.ExceptionExtension;
using G.Infrastructure.Contract.Deploy.Model;
using G.Service.Model.DeploySlave.Deploy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace G.Service.Contract.DeploySlave.Interfaces
{
    [ServiceContract]
    public interface IDeploySlave
    {
        [OperationContract]
        [FaultContract(typeof(FaultMessage))]
        void DownloadFilePackage(FilePackageInfo filePackageInfo);

        [OperationContract]
        [FaultContract(typeof(FaultMessage))]
        void Deploy(DeployInfo deployInfo);
    }
}
