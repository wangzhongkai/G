﻿using Framework.Wcf.ExceptionExtension;
using G.Infrastructure.Contract.Deploy.Model;
using G.Service.Model.DeploySlave.ProcessManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace G.Service.Contract.DeploySlave.Interfaces
{
    [ServiceContract]
    public interface IProcessManage
    {
        [OperationContract]
        [FaultContract(typeof(FaultMessage))]
        int Run(RunProcessInfo runProcessInfo);

        [OperationContract]
        [FaultContract(typeof(FaultMessage))]
        bool IsRunning(CheckProcessStatusInfo checkProcessStatusInfo);

        [OperationContract]
        [FaultContract(typeof(FaultMessage))]
        bool Kill(KillProcessInfo killProcessInfo);
    }
}
