@echo off
echo Start to Publish G.Service.Contract.DeploySlave
set local=".."
Set RemotePath=C$\ProjectInstance\G.Service.Contract.DeploySlave

For %%I in (192.168.0.1 192.168.0.2) do (
echo "Copy %%I"
net use \\%%I\ipc$ password /User:username
For %%P in (default) do (
::echo "Publish xcopy /e /y %local% "\\%%I\%RemotePath%\""
xcopy /e /y %local% "\\%%I\%RemotePath%\"
)
net use \\%%I\ipc$ /del
echo "install %%I windows service"
sc \\%%I create G.Service.Contract.DeploySlave binPath="C:\ProjectInstance\G.Service.Contract.DeploySlave"
echo "set config %%I windows service"
sc \\%%I config G.Service.Contract.DeploySlave start=AUTO
echo "start %%I windows service"
sc \\%%I start G.Service.Contract.DeploySlave
)

:End

Pause