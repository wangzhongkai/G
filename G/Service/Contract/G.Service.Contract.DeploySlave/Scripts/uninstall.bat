@echo off
echo Start to Publish G.Service.Contract.DeploySlave
set local=".."
Set RemotePath=C$\ProjectInstance\G.Service.Contract.DeploySlave

For %%I in (192.168.0.1 192.168.0.2) do (
echo "stop %%I windows service"
sc \\%%I stop G.Service.Contract.DeploySlave
echo "delete %%I windows service"
sc \\%%I delete G.Service.Contract.DeploySlave
net use \\%%I\ipc$ password /User:username
RD /S /Q "\\%%I\%RemotePath%\"
net use \\%%I\ipc$ /del
)

:End

Pause