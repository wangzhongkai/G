@echo off
echo Start to Publish G.Service.Contract.DeploySlave
set local=".."
Set RemotePath=C$\ProjectInstance\G.Service.Contract.DeploySlave

For %%I in (192.168.0.1 192.168.0.2) do (
echo "stop %%I windows service"
sc \\%%I stop G.Service.Contract.DeploySlave
echo "Copy %%I"
net use \\%%I\ipc$ password /User:username
For %%P in (default) do (
::echo "Publish xcopy /e /y %local% "\\%%I\%RemotePath%\""
xcopy /e /y %local% "\\%%I\%RemotePath%\"
)
net use \\%%I\ipc$ /del
echo "start %%I windows service"
sc \\%%I start G.Service.Contract.DeploySlave
)

:End

Pause