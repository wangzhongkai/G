﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace G.Service.Business.DeploySlave.Helpers
{
    public class DeployFilePackageHelper
    {
        public static string GetDeployFilePackagePath(string filePackageName)
        {
            var rootDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var deployDirectory = Path.Combine(rootDirectory, "Download", "Deploy");

            return Path.Combine(deployDirectory, filePackageName);
        }
    }
}
