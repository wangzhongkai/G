﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using G.Infrastructure.Windows.ProcessOperation;
using G.Service.Model.DeploySlave.ProcessManage;

namespace G.Service.Business.DeploySlave
{
    public class ProcessManageBusiness
    {
        public int Run(RunProcessInfo runProcessInfo)
        {
            return new ProcessWrapper().Run(
                runProcessInfo.ExeFilePath,
                runProcessInfo.Arguments,
                runProcessInfo.HideWindow);
        }

        public bool IsRunning(CheckProcessStatusInfo checkProcessStatusInfo)
        {
            return new ProcessWrapper().GetProcessInfoList(checkProcessStatusInfo.ExeFilePath).SingleOrDefault(p => p.Id == checkProcessStatusInfo.ProcessID) != null;
        }

        public bool Kill(KillProcessInfo killProcessInfo)
        {
            var process = new ProcessWrapper().GetProcessInfoList(killProcessInfo.ExeFilePath).SingleOrDefault(p => p.Id == killProcessInfo.ProcessID);

            if (process == null)
                return false;

            process.Kill();
            process.WaitForExit();

            return true;
        }
    }
}
