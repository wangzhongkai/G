﻿using G.Infrastructure.Contract.Deploy.Model;
using G.Infrastructure.Plugin;
using G.Service.Model.DeploySlave.Deploy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace G.Service.Business.DeploySlave
{
    public class DeploySlaveBusiness
    {
        public void DownloadFilePackage(FilePackageInfo filePackageInfo)
        {
            var rootDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var saveDirectory = Path.Combine(rootDirectory, "Download", "Deploy");

            if (!Directory.Exists(saveDirectory))
            {
                Directory.CreateDirectory(saveDirectory);
            }

            var savePath = Path.Combine(saveDirectory, filePackageInfo.Name);

            using (var wc = new WebClient())
            {
                if (!string.IsNullOrWhiteSpace(filePackageInfo.Token))
                    wc.Headers.Add("Authorization", $"Basic {filePackageInfo.Token}");

                wc.DownloadFile(filePackageInfo.Url, savePath);
            }
        }

        public void Deploy(DeployInfo deployInfo)
        {
            var deployHost = DeployHostBuilder.Get(deployInfo.DeployHostType);
            deployHost.Deploy(deployInfo);
        }
    }
}
