﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Service.Model.DeploySlave.ProcessManage
{
    public class CheckProcessStatusInfo
    {
        public int ProcessID { get; set; }

        public string ExeFilePath { get; set; }
    }
}
