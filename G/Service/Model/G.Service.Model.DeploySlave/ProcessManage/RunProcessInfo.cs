﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Service.Model.DeploySlave.ProcessManage
{
    public class RunProcessInfo
    {
        public string ExeFilePath { get; set; }

        public string Arguments { get; set; }

        public bool HideWindow { get; set; }
    }
}
