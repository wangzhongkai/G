﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G.Service.Model.DeploySlave.Deploy
{
    public class FilePackageInfo
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string Token { get; set; }
    }
}
